<div class="home-slider position-relative overflow-hidden">
<div id="sliderSyncingNav" class="js-slick-carousel slick" data-hs-slick-carousel-options='{
                    "dots": true,
                    "dotsClass": "d-xl-none slick-pagination mb-3 position-absolute right-0 left-0 bottom-0",
                    "infinite": true,
                    "asNavFor": "#sliderSyncingThumb"
                }'>
<!--<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/2048x1074/Temple_Homepage_Rotator.jpg);">-->
<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/2048x1074/1.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<!--<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">American Made</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
<li class="">2020</li>
<li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>-->
<div class="d-md-flex">
<a href="video.php" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="video.php" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>
<!--<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/2048x1074/TheCurseOfOakIsland_Homepage_Rotator.jpg);">-->
<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/2048x1074/2.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">The Convenient Groom</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
 <li class="">2020</li>
<li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>
<div class="d-md-flex">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>
<!--<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/2048x1074/RHWOF_SaltLakeCity_Homepage_Rotator.jpg);">-->
<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/2048x1074/3.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">Pacific Rim: Uprising</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
<li class="">2020</li>
<li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>
<div class="d-md-flex">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>
<!--<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/2048x1074/Departure_RotatorLarge_v4.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">Black Mirror</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
<li class="">2020</li>
<li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>
<div class="d-md-flex">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>
<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/1920x676/img5.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">Paradigm Lost</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
<li class="">2020</li>
 <li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>
<div class="d-md-flex">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>
<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/1920x676/img6.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">Black Mirror</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
<li class="">2020</li>
<li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>
<div class="d-md-flex">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>
<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/1920x676/img7.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">Fantastic Beasts and Where to Find Them</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
<li class="">2020</li>
<li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>
<div class="d-md-flex">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>
<div class="bg-img-hero d-flex align-items-center min-h-676rem slider-gradient" style="background-image: url(assets/img/1920x676/img8.jpg);">
<div class="container">
<div class="mx-3 mx-md-5  col-xl-5 px-0">
<h1 class="display-3 mb-3"><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="h-w-primary">American Made</a></h1>
<div class="mb-4d">
<ul class="list-unstyled nav nav-meta nav-meta__white">
<li class="">2020</li>
<li class=""><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html">Comedy</a></li>
<li class="">1hr 55 mins</li>
</ul>
</div>
<div class="d-md-flex">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-primary px-6 py-3d btn-sm mb-3 mb-md-0">WATCH NOW</a>
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v2.html" class="btn btn-outline-light ml-md-4 px-6 py-3d btn-sm">+ PLAYLIST</a>
</div>
</div>
</div>
</div>-->
</div>
<div class="d-none d-xl-flex position-xl-absolute col-xl-6 mr-3 right-0 bottom-0  mb-xl-12  slider-m-0 flex-column">
<h4 class="text-white font-size-22 font-weight-normal mb-3">Todays Recomendation</h4>
<div id="sliderSyncingThumb" class="js-slick-carousel slick slick-gutters-1 slick-transform-off" data-hs-slick-carousel-options='{
                        "infinite": true,
                        "slidesToShow": 8,
                        "isThumbs": true,
                        "asNavFor": "#sliderSyncingNav",
                        "responsive": [{
                            "breakpoint": 1199,
                            "settings": {
                                "slidesToShow": 2
                            },
                            "breakpoint": 768,
                            "settings": {
                                "slidesToShow": 2
                            }
                        }]
                    }'>
<!--<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img1.jpg" alt="Image Description">
</div>
<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img2.jpg" alt="Image Description">
</div>
<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img3.jpg" alt="Image Description">
</div>
<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img4.jpg" alt="Image Description">
</div>
<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img5.jpg" alt="Image Description">
</div>
<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img6.jpg" alt="Image Description">
</div>
<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img7.jpg" alt="Image Description">
</div>
<div class="my-1d" style="cursor: pointer;">
<img class="img-fluid" style="width:204px; height:120px;" src="assets/img/204x120/img8.jpg" alt="Image Description">
</div>-->
</div>
</div>
</div>