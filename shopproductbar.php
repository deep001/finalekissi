<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\ChannelLogin;
use App\Classes\Audio;
use App\Classes\Cart;
use App\Classes\Csrf;
use App\Classes\Headers;
use App\Classes\Country;

$_SESSION['url'] = $_SERVER['REQUEST_URI'];
/* $userid = $_SESSION['uid'];
 $id = $_SESSION['id'];
 $inv = $_SESSION['inv']; */

if(isset($_GET['productid']) && !empty($_GET['productid']))
{
	$productid = base64_decode($_GET['productid']);
    $audio = new Audio();
    $getbookslist = $audio->getBooksCompleteInfo($productid);
//	print_r($getbookslist);die;
}

$bookslist = new Audio();
$getbookslistrelated = $bookslist->getChannelfifthBooksListasc();


$countrylist = new Country();
$getcountrylistrecord = $countrylist->getCountryList();

if(isset($_POST['addtocart']))
{
	
	
	
if(isset($_SESSION['id']))
{
	
		  
		     $addtocart = new Cart();
             $update = $addtocart->checkProductAdd($_POST);
	if($update->status == true)
	{
		 Headers::redirect("shop-checkout.php"); 
	}
	 
		     
    }

else
{ 
?>
<script type="text/javascript">
alert("Please login!"); window.location.href='signin.php'
</script>
<?php
}
}

$getcsrf = Csrf::getCsrfToken();
$_SESSION['csrf'] = $getcsrf;
?>
<!DOCTYPE html>
<html lang="zxx">
<head>

<title>Shop Product Page | Ocean TV</title>

<?php include_once Config::path()->INCLUDE_PATH.'/oceanfronthead.php'; ?>
	



</head>
<body>

<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontheadernew.php'; ?>


<main id="content">
<div class="bg-gray-1100 space-bottom-2 space-bottom-lg-3">
<div class="bg-img-hero position-relative" style="background-image: url(assets/img/1920x343/img1.jpg); height: 350px;"></div>
<section>
<div class="container px-md-6">
	
	  <h4>
                        <?php
						echo (isset($validationcsrf->msg))? '<div class="alert alert-primary" style="color:red;">'.$validationcsrf->msg.'</div>':'';
					//	echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
                        </h4>
<form class="" name="cart" id="cart" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" onSubmit="return val();">
<div class="row">
<div class="col-md-3">
<div class="mt-n8">
<img class="img-fluid" src="assets/img/122x183/<?php echo $getbookslist->audioimage;?>" alt="Image-Description">
	<input type="hidden" value="<?php echo $getbookslist->audioimage; ?>" name="productimage" />
	<input type="hidden" name="csrf-token" value="<?php echo $_SESSION['csrf']; ?>">
	<input type="hidden" name="prid" value="<?php echo base64_decode($_GET['productid']); ?>">
	<input type="hidden" name="userid" value="<?php echo $_SESSION['uid']; ?>">
	<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
	<input type="hidden" name="inv" value="<?php echo $_SESSION['inv']; ?>">
</div>
<div class="bg-gray-3100">
<div>
<ul class="list-unstyled">
<li class="border-bottom border-gray-3200">
<div class="p-4">
<a class="d-block mt-1" href="#">
<img class="img-fluid" src="assets/img/200x40/img1.png" alt="Image-Description">
</a>
</div>
</li>
<li class="border-bottom border-gray-3200">
<div class="p-4">
<a class="d-block mt-1" href="#">
<img class="img-fluid" src="assets/img/200x40/img2.png" alt="Image-Description">
</a>
</div>
</li>
<li class="border-bottom border-gray-3200">
<div class="p-4">
<a class="d-block mt-1" href="#">
<img class="img-fluid" src="assets/img/200x40/img3.png" alt="Image-Description">
</a>
</div>
</li>
<li class="border-bottom border-gray-3200">
<div class="p-4">
<a class="d-block mt-1" href="#">
<img class="img-fluid" src="assets/img/200x40/img4.png" alt="Image-Description">
</a>
</div>
</li>
</ul>
</div>
</div>
</div>
<div class="col-md-9">
<div class="pl-md-2 pt-md-5">
<div class="row border-bottom border-gray-5600 space-bottom-2 no-gutters mb-4">
<div class="col-md-7 col-lg">
<div class="mb-5 mb-md-0">
<h6 class="font-size-36 text-white mb-4 pb-1"><?php echo $getbookslist->audioname;?></h6>
	<input type="hidden" value="<?php echo $getbookslist->audioname; ?>" name="productname" />
<!--<div class="d-flex align-items-center mb-5">
<div class="d-flex">
<div>
<i class="fas fa-star text-primary font-size-42"></i>
</div>
<div class="text-lh-1 ml-1">
<div class="text-primary font-size-24 font-weight-semi-bold">9.0</div>
<span class="text-gray-1300 font-size-12">2 Votes</span>
</div>
</div>
<div class="d-flex align-items-center ml-6 text-gray-1300">
<div>
<i class="far fa-heart font-size-30"></i>
</div>
<a href="#" class="text-gray-1300 ml-2">+ Playlist</a>
</div>
</div>-->
<!--<ul class="list-unstyled nav nav-meta font-secondary mb-3 pb-1 flex-nowrap flex-lg-wrap overflow-auto overflow-lg-hidden">
<li class="text-white flex-shrink-0 flex-shrink-lg-1">2020</li>
<li class="text-white flex-shrink-0 flex-shrink-lg-1">1hr 24 mins</li>
<li class="text-white flex-shrink-0 flex-shrink-lg-1">R</li>
<li class="text-white flex-shrink-0 flex-shrink-lg-1">
<a href="#" class="text-white">Action</a>
<span>,</span>
<a href="#" class="text-white">Documentary</a>
</li>
</ul>-->
<p class="text-white font-size-16 text-lh-lg mb-5 pb-1"><?php echo $getbookslist->audiodescription;?></p>
	<input type="hidden" value="<?php echo $getbookslist->audiodescription; ?>" name="productdescription" />
	<p class="text-white font-size-16 text-lh-lg mb-5 pb-1">$<?php echo $getbookslist->price;?></p>
	<input type="hidden" name="pr" id="pr" value="<?php echo $getbookslist->price;?>">
	<p class="text-white font-size-16 text-lh-lg mb-5 pb-1">

	 Country for shipping:<select name="shippingcountry" id="shippingcountry" class="form-control rounded-0">
		<?php 	foreach($getcountrylistrecord as $roww) {  ?>
		                  <option value="<?php echo $roww->countryname; ?>"><?php echo $roww->countryname; ?></option>
		<?php } ?>
		                  </select>
	</p>
	<div id="tabs-2" style="display:none;">Shipping price : </div>
	
	<p class="text-white font-size-16 text-lh-lg mb-5 pb-1">

	 Quantity:<input type="text" class="form-control rounded-0" id="qty" name="qty" value="" >
	</p>
	<p class="text-white font-size-16 text-lh-lg mb-5 pb-1">

	 Total:$<input type="text" class="form-control rounded-0" id="total" name="total" value="">
	</p>
	<p class="text-white font-size-16 text-lh-lg mb-5 pb-1">

	 Final Cost for shipping:$<input type="text" class="form-control rounded-0" id="finalshipping" name="finalshipping" value="">
	</p>
	
<!--<div class="d-flex justify-content-between">
<div>
<h6 class="font-size-15 mb-0">
<a href="../single-movies/single-movies-v7.html" class="text-white">Robert Rodriguez</a>
</h6>
<span class="text-white font-size-13">Director</span>
</div>
<div>
<h6 class="font-size-15 mb-0">
<a href="../single-movies/single-movies-v7.html" class="text-white">Martin McKandy</a>
</h6>
<span class="text-white font-size-13">Screenplay</span>
</div>
<div>
<h6 class="font-size-15 mb-0">
<a href="../single-movies/single-movies-v7.html" class="text-white">Anna martez</a>
</h6>
<span class="text-white font-size-13">Producer</span>
</div>
</div>-->
</div>
</div>
<div class="col-md col-lg-auto">
<div class="pl-lg-3">
<div class="d-flex flex-column">
<a href="#" class="btn btn-primary d-flex align-items-center justify-content-center w-lg-220rem h-52rem mb-3" tabindex="0">WATCH NOW</a>
<!--<a href="#" class="btn btn-primary d-flex align-items-center justify-content-center  w-lg-220rem h-52rem mb-3" tabindex="0">ADD TO CART</a>-->
<span class="posted_in"> <input type="submit" class="btn btn-primary d-flex align-items-center justify-content-center  w-lg-220rem h-52rem mb-3" name="addtocart" value="ADD TO CART" tabindex="0"></span>
	
<!--<a href="#" class="btn btn-outline-light d-flex align-items-center justify-content-center  w-lg-220rem h-52rem" tabindex="0">+ PLAYLIST</a>-->
</div>
</div>
</div>
</div>
<div class="mb-7">
<div class="font-size-26 text-gray-5500 mb-3 pb-1">Related Books</div>
<div class="row dark row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-6">
	<?php 	foreach($getbookslistrelated as $roww) {  ?>
<div class="col">
<div class="product mb-4 mb-xl-0">
<div class="product-image mb-2">
<a href="shopproductbar.php?productid=<?php echo base64_encode($roww->audioid);?>">
<img class="img-fluid" src="assets/img/122x183/<?php echo $roww->audioimage;?>" alt="Image-Description">
</a>
</div>
<div>
<h6 class="font-size-1 mb-0 product-title">
<a href="shopproductbar.php?productid=<?php echo base64_encode($roww->audioid);?>"><?php echo $roww->audioname;?></a>
</h6>
<span class="text-gray-5700 font-size-12"><?php echo $roww->audiodescription;?></span>
</div>
</div>
</div>
	<?php } ?>

</div>
</div>
<!--<div class="mb-6">
<div class="font-size-26 text-gray-5500 mb-3 pb-1">Trailers & Clips</div>
<div class="row dark mx-n2">
<div class="col-md-6 col-lg-3 px-2">
<div class="product mb-5 mb-lg-0">
<div class="product-image mb-2 pb-1">
<a href="../single-movies/single-movies-v7.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/480x270/img66.jpg" alt="Image Description">
</a>
</div>
<div>
<div class="font-size-1 mb-1 product-title font-weight-bold">
<a href="../single-movies/single-movies-v7.html">Robot Car</a>
</div>
<div class="font-size-12 text-gray-1300">
<span>35 views</span>
<span class="product-comment">1 year ago</span>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 px-2">
<div class="product mb-5 mb-lg-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v7.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/480x270/img67.jpg" alt="Image Description">
</a>
</div>
<div>
<div class="font-size-1 mb-1 product-title font-weight-bold">
<a href="../single-movies/single-movies-v7.html">Hidden Agenda</a>
</div>
<div class="font-size-12 text-gray-1300">
<span>135 views</span>
<span class="product-comment">1 year ago</span>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 px-2">
<div class="product mb-5 mb-md-0">
 <div class="product-image mb-2">
<a href="../single-movies/single-movies-v7.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/480x270/img68.jpg" alt="Image Description">
</a>
</div>
<div>
<div class="font-size-1 mb-1 product-title font-weight-bold">
<a href="../single-movies/single-movies-v7.html">Fans energy before events</a>
</div>
<div class="font-size-12 text-gray-1300">
<span>5 views</span>
<span class="product-comment">1 year ago</span>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 px-2">
<div class="product mb-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v7.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/480x270/img62.jpg" alt="Image Description">
</a>
</div>
<div>
<div class="font-size-1 mb-1 product-title font-weight-bold">
<a href="../single-movies/single-movies-v7.html">Shark Attack</a>
</div>
<div class="font-size-12 text-gray-1300">
<span>34 views</span>
<span class="product-comment">1 year ago</span>
</div>
</div>
</div>
</div>
</div>
</div>-->
<!--<div class="mb-6 mb-lg-9 pb-lg-1">
<div class="font-size-26 text-gray-5500 mb-3 pb-1">Our Review</div>
<div>
<div class="d-flex mb-3 pb-1">
<div>
<img class="img-fluid rounded-circle" src="assets/img/36x36/img1.jpg" alt="Image-Description">
</div>
<div class="ml-3">
<div class="mb-2">
<span class="text-primary">nilofer</span>
<span class="text-gray-1300 ml-1">March 1, 2020</span>
</div>
<div class="form-group d-flex align-items-center justify-content-between font-size-15 text-gray-1300 text-lh-lg text-body mb-0">
<span class="d-block text-gray-1300">
 <i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="far fa-star"></i>
<i class="far fa-star"></i>
<i class="far fa-star"></i>
</span>
</div>
</div>
</div>
<p class="mb-3 text-gray-5500">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
<div class="text-gray-1300">
<a href="#" class="text-gray-1300">
<i class="far fa-thumbs-up font-size-18"></i>
</a>
<span class="font-size-12 font-weight-semi-bold ml-1">1+</span>
</div>
</div>
</div>-->
<!--<div class="mb-6 mb-lg-8 pb-lg-1">
<div class="row">
<div class="col-lg-6">
<div class="mb-5 mb-lg-0">
<div class="font-size-26 text-gray-5500 mb-3 pb-1">Details Of Movie</div>
<table>
<tbody>
<tr>
<th class="text-gray-5500 font-weight-normal">Music</th>
<td><span class="font-weight-medium"><a href="#">Alan Silvestri</a></span></td>
</tr>
<tr>
<th class="text-gray-5500 w-160rem font-weight-normal pb-6">Photos</th>
<td><span class="d-block font-weight-medium"><a href="#">Jack Kirby</a></span> <span class="d-block font-weight-medium"> <a href="#">Jim Starlin</a></span> <span class="font-weight-medium"> <a href="#">Trent Opaloch</a></span></td>
</tr>
<tr>
<th class="text-gray-5500 w-160rem font-weight-normal pb-4">Boxoffice</th>
<td><span class="d-block text-gray-5500 font-weight-medium">$1 845 482 612 out of USA</span><span class="text-gray-5500 font-weight-medium"> $771 368 375 in USA</span></td>
</tr>
 <tr>
<th class="text-gray-5500 w-160rem font-weight-normal">Location</th>
<td><span class="text-gray-5500 font-weight-medium">USA</span></td>
</tr>
<tr>
<th class="text-gray-5500 w-160rem font-weight-normal">Producer</th>
<td><span class="text-gray-5500 font-weight-medium">Nerovision Co</span></td>
</tr>
<tr>
<th class="text-gray-5500 w-160rem font-weight-normal">Director</th>
<td><span class="text-gray-5500 font-weight-medium">M. Night Shyamalan</span></td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="col-lg-6">
<div class="description-title font-size-26 text-gray-5500 mb-3 pb-1">Fun Facts of Movie</div>
<p class="text-gray-5500 mb-0 text-lh-md">Praesent iaculis, purus ac vehicula mattis, arcu lorem blandit nisl, non laoreet dui mi eget elit. Donec porttitor ex vel augue maximus luctus. Vivamus finibus nibh eu nunc volutpat suscipit. Nam vulputate libero quis nisi euismod rhoncus. Sed eu euismod felis. Aenean ullamcorper dapibus odio ac tempor. Aliquam iaculis, quam vitae imperdiet consectetur, mi ante semper metus, ac efficitur nisi justo ut eros.</p>
</div>
</div>
</div>-->
<!--<div class="mb-6">
<div class="description-title font-size-26 text-gray-5500 mb-3 pb-1">User Reviews</div>
<div class="border-bottom border-gray-5600 mb-4 pb-4">
<div class="d-flex mb-3 pb-1">
<div>
<img class="img-fluid rounded-circle" src="assets/img/36x36/img1.jpg" alt="Image-Description">
</div>
<div class="ml-3">
<div class="mb-2">
<span class="text-primary">Nilofer</span>
<span class="text-gray-1300 ml-1">March 1, 2020</span>
</div>
<div class="form-group d-flex align-items-center justify-content-between font-size-15 text-gray-1300 text-lh-lg text-body mb-0">
<span class="d-block text-gray-1300">
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="far fa-star"></i>
<i class="far fa-star"></i>
</span>
</div>
</div>
</div>
<p class="mb-3 text-gray-5500">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
<div class="text-gray-1300">
<a href="#" class="text-gray-1300">
<i class="far fa-thumbs-up font-size-18"></i>
</a>
<span class="font-size-12 font-weight-semi-bold ml-1">1+</span>
</div>
</div>
<div>
<div class="d-flex mb-3 pb-1">
<div>
<img class="img-fluid rounded-circle" src="assets/img/36x36/img1.jpg" alt="Image-Description">
</div>
<div class="ml-3">
<div class="mb-2">
<span class="text-primary">admin</span>
<span class="text-gray-1300 ml-1">March 1, 2020</span>
</div>
<div class="form-group d-flex align-items-center justify-content-between font-size-15 text-gray-1300 text-lh-lg text-body mb-0">
<span class="d-block text-gray-1300">
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
</span>
</div>
</div>
</div>
<p class="mb-3 text-gray-5500">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
<div class="text-gray-1300">
<a href="#" class="text-gray-1300">
<i class="far fa-thumbs-up font-size-18"></i>
</a>
<span class="font-size-12 font-weight-semi-bold ml-1">0</span>
</div>
</div>
</div>-->
<!--<div>
<div class="home-section">
<header class="d-md-flex align-items-center justify-content-between mb-3 pb-1 w-100">
<h6 class="section-title-dark d-block position-relative font-size-18 font-weight-medium overflow-md-hidden m-0 text-white flex-grow-1">Add a review</h6>
</header>
</div>
<div class="text-gray-1300">You must be
<a href="#">logged in</a> to post a review.
</div>
</div>-->
</div>
</div>
</div>
	</form>
</div>
</section>
</div>
</main>


<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontfooter.php'; ?>



<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<button type="button" class="close position-absolute top-0 right-0 z-index-2 mt-3 mr-3" data-dismiss="modal" aria-label="Close">
<svg aria-hidden="true" class="mb-0" width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</button>

<div class="modal-body">
<form class="js-validate">

<div id="login">

<div class="text-center mb-7">
<h3 class="mb-0">Sign In to Vodi</h3>
<p>Login to manage your account.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signinEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-3">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signinPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
</div>
</div>

<div class="d-flex justify-content-end mb-4">
<a class="js-animation-link small link-underline" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#forgotPassword",
                                        "groupName": "idForm"
                                    }'>Forgot Password?
</a>
</div>
<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign In</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
Sign In with Google
</span>
</a>
<div class="text-center">
<span class="small text-muted">Do not have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#signup",
                                        "groupName": "idForm"
                                    }'>Sign Up
</a>
</div>
</div>

<div id="signup" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Create your account</h3>
<p>Fill out the form to get started.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signupEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signupPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Confirm Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="confirmPassword" id="signupConfirmPassword" placeholder="Confirm Password" aria-label="Confirm Password" required data-msg="Password does not match the confirm password.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign Up</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
Sign Up with Google
</span>
 </a>
<div class="text-center">
<span class="small text-muted">Already have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Sign In
</a>
</div>
</div>


<div id="forgotPassword" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Recover password</h3>
<p>Instructions will be sent to you.</p>
</div>


<div class="js-form-message">
<label class="sr-only" for="recoverEmail">Your email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="recoverEmail" placeholder="Your email" aria-label="Your email" required data-msg="Please enter a valid email address.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Recover Password</button>
</div>
<div class="text-center mb-4">
<span class="small text-muted">Remember your password?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Login
</a>
</div>
</div>

</form>
</div>

</div>
</div>
</div>


<aside id="sidebarContent" class="hs-unfold-content sidebar sidebar-left off-canvas-menu">
<div class="sidebar-scroller">
<div class="sidebar-container">
<div class="sidebar-footer-offset" style="padding-bottom: 7rem;">

<div class="d-flex justify-content-end align-items-center py-2 px-4 border-bottom">

<a class="navbar-brand mr-auto" href="../home/index.html" aria-label="Vodi">
<svg version="1.1" width="103" height="40px">
<linearGradient id="vodi-gr1" x1="0%" y1="0%" x2="100%" y2="0%">
<stop offset="0" style="stop-color:#2A4999"></stop>
<stop offset="1" style="stop-color:#2C9CD4"></stop>
</linearGradient>
<g class="vodi-gr">
<path class="vodi-svg0" d="M72.8,12.7c0-2.7,0-1.8,0-4.4c0-0.9,0-1.8,0-2.8C73,3,74.7,1.4,77,1.4c2.3,0,4.1,1.8,4.2,4.2c0,1,0,2.1,0,3.1
                                    c0,6.5,0,9.4,0,15.9c0,4.7-1.7,8.8-5.6,11.5c-4.5,3.1-9.3,3.5-14.1,0.9c-4.7-2.5-7.1-6.7-7-12.1c0.1-7.8,6.3-13.6,14.1-13.2
                                    c0.7,0,1.4,0.2,2.1,0.3C71.3,12.2,72,12.4,72.8,12.7z M67.8,19.8c-2.9,0-5.2,2.2-5.2,5c0,2.9,2.3,5.3,5.2,5.3
                                    c2.8,0,5.2-2.4,5.2-5.2C73,22.2,70.6,19.8,67.8,19.8z
                                    M39.9,38.6c-7.3,0-13.3-6.1-13.3-13.5c0-7.5,5.9-13.4,13.4-13.4c7.5,0,13.4,6,13.4,13.5
                                    C53.4,32.6,47.4,38.6,39.9,38.6z M39.9,30.6c3.2,0,5.6-2.3,5.6-5.6c0-3.2-2.3-5.5-5.5-5.5c-3.2,0-5.6,2.2-5.6,5.4
                                    C34.4,28.2,36.7,30.6,39.9,30.6z
                                    M14.6,27c0.6-1.4,1.1-2.6,1.6-3.8c1.2-2.9,2.5-5.8,3.7-8.8c0.7-1.7,2-2.8,4-2.7c3,0,4.9,2.6,3.8,5.4
                                    c-0.5,1.3-1.2,2.6-1.8,3.9c-2.4,5-4.9,10-7.3,15c-0.8,1.6-2,2.6-3.9,2.6c-2,0-3.3-0.8-4.2-2.6c-2.7-5.6-5.3-11.1-8-16.7
                                    c-0.3-0.7-0.6-1.3-0.9-2c-0.8-1.8-0.3-3.7,1.1-4.8c1.5-1.2,4-1.3,5.3,0c0.7,0.6,1.2,1.5,1.6,2.3C11.3,18.8,12.9,22.7,14.6,27z
                                    M90.9,25.1c0,3.1,0,6.2,0,9.4c0,1.9-1.2,3.4-2.9,4c-1.7,0.5-3.5,0-4.5-1.6c-0.5-0.8-0.8-1.8-0.8-2.6
                                    c-0.1-6.1-0.1-11.3,0-17.5c0-2.2,1.5-3.9,3.5-4.2c2.1-0.3,4.1,0.9,4.7,2.9c0.1,0.5,0.2,1.1,0.2,1.6C90.9,20,90.9,22.1,90.9,25.1
                                    C90.9,25.1,90.9,25.1,90.9,25.1z
                                    M90.2,4.7L86,2.3c-1.3-0.8-3,0.2-3,1.7v4.8c0,1.5,1.7,2.5,3,1.7l4.2-2.4C91.5,7.4,91.5,5.5,90.2,4.7z"></path>
</g>
</svg>
</a>

<div class="hs-unfold">
<a class="js-hs-unfold-invoker btn btn-icon btn-xs btn-soft-secondary" href="javascript:;" data-hs-unfold-options='{
                                    "target": "#sidebarContent",
                                    "type": "css-animation",
                                    "animationIn": "fadeInLeft",
                                    "animationOut": "fadeOutLeft",
                                    "hasOverlay": "rgba(55, 125, 255, 0.1)",
                                    "smartPositionOff": true
                                }'>
<svg width="10" height="10" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</a>
</div>
</div>


<div class="scrollbar sidebar-body">
<div class="sidebar-content py-4">

<div class="">
<div id="sidebarNavExample3" class="collapse show navbar-collapse">

<div class="sidebar-body_inner">
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav1Collapse" data-target="#sidebarNav1Collapse">
Home
</a>
<div id="sidebarNav1Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav1" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../home/index.html">Home v1</a>
<a class="dropdown-item" href="../home/home-v2.html">Home v2</a>
<a class="dropdown-item" href="../home/home-v3.html">Home v3</a>
<a class="dropdown-item" href="../home/home-v4.html">Home v4</a>
<a class="dropdown-item" href="../home/home-v5.html">Home v5</a>
<a class="dropdown-item" href="../home/home-v6.html">Home v6 - Vodi Prime (Light)</a>
<a class="dropdown-item" href="../home/home-v7.html">Home v7 - Vodi Prime (Dark)</a>
<a class="dropdown-item" href="../home/home-v8.html">Home v8 - Vodi Stream</a>
<a class="dropdown-item" href="../home/home-v9.html">Home v9 - Vodi Tube (Light)</a>
<a class="dropdown-item" href="../home/home-v10.html">Home v10 - Vodi Tube (Dark)</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Collapse" data-target="#sidebarNav2Collapse">
Archive Pages
</a>
<div id="sidebarNav2Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav2" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../archive/movies.html">Movies</a>
<a class="dropdown-item" href="../archive/tv-shows.html">TV Shows</a>
<a class="dropdown-item" href="../archive/videos.html">Videos</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2One">
Single Movies
</a>
<div id="sidebarNav2One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-movies/single-movies-v1.html">Movie v1</a>
<a class="dropdown-item" href="../single-movies/single-movies-v2.html">Movie v2</a>
<a class="dropdown-item" href="../single-movies/single-movies-v3.html">Movie v3</a>
<a class="dropdown-item" href="../single-movies/single-movies-v4.html">Movie v4</a>
<a class="dropdown-item" href="../single-movies/single-movies-v5.html">Movie v5</a>
<a class="dropdown-item" href="../single-movies/single-movies-v6.html">Movie v6</a>
 <a class="dropdown-item" href="../single-movies/single-movies-v7.html">Movie v7</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Two">
Single Videos
</a>
<div id="sidebarNav2Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-video/single-video-v1.html">Video v1</a>
<a class="dropdown-item" href="../single-video/single-video-v2.html">Video v2</a>
<a class="dropdown-item" href="../single-video/single-video-v3.html">Video v3</a>
<a class="dropdown-item" href="../single-video/single-video-v4.html">Video v4</a>
<a class="dropdown-item" href="../single-video/single-video-v5.html">Video v5</a>
<a class="dropdown-item" href="../single-video/single-video-v6.html">Video v6</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Collapse" data-target="#sidebarNav3Collapse">
Single Episodes
</a>
<div id="sidebarNav3Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav3" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../single-episodes/single-episodes-v1.html">Episode v1</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v2.html">Episode v2</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v3.html">Episode v3</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v4.html">Episode v4</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3One">
Other Pages
</a>
<div id="sidebarNav3One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../other/landing-v1.html">Landing v1</a>
<a class="dropdown-item" href="../other/landing-v2.html">Landing v2</a>
<a class="dropdown-item" href="../other/coming-soon.html">Coming Soon</a>
<a class="dropdown-item" href="../single-tv-show/single-tv-show.html">Single TV Show</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Two">
Blog Pages
</a>
<div id="sidebarNav3Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../blog/blog.html">Blog</a>
<a class="dropdown-item" href="../blog/blog-single.html">Single Blog</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4Collapse" data-target="#sidebarNav4Collapse">
Static Pages
</a>
<div id="sidebarNav4Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav4" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../static/contact.html">Contact Us</a>
<a class="dropdown-item" href="../static/404.html">404</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav4One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4One">
Docs
</a>
<div id="sidebarNav4One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav4">
<a class="dropdown-item" href="../../documentation/index.html">Documentation</a>
<a class="dropdown-item" href="../../snippets/index.html">Snippets</a>
</div>
</div>

</div>
</div>
</div>

</div>
</div>

</div>
</div>
</div>
</aside>



<a class="js-go-to go-to position-fixed" href="javascript:;" style="visibility: hidden;" data-hs-go-to-options='{
            "offsetTop": 700,
            "position": {
                "init": {
                    "right": 15
                },
                "show": {
                    "bottom": 15
                },
                "hide": {
                    "bottom": -15
                }
            }
        }'>
<i class="fas fa-angle-up"></i>
</a>


<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontscript.php'; ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script type="text/javascript">
		
		 $(document).ready(function(){
	   $("#qty").keyup(function(){
     var qt = $("#qty").val();
	 var ship = $("#ship").val();
	
		   if(qt<=0 || qt>5)
		   {
			   alert("Maximum 5 product can be entered");
			   return false;
		   }
		else {
	var pr = $("#pr").val();	
		
		   var tot = qt*pr;
		   var finalship = qt*ship;
		  //    alert("total is"+tot);
		//   $("#total").val() = tot;
		    $('#total').val(tot);
			$('#finalshipping').val(finalship);
		   return true;
		}
          });
			 
			 	 
		  }); 
	</script>
			 
	<script type="text/javascript">
		$(document).ready(function(){
	$("#shippingcountry").on('change',function(){
		
		
		var shippingcountry = $(this).val();
        if(shippingcountry){
        $.ajax({
        type:'POST',
        url: 'showshippingprice.php',
        data:'shippingcountry='+shippingcountry,
        success:function(result){
      //  $('#tabs-2').html(result);
			$( "#tabs-2" ).html(result).show(); 

}

});

}
		
	});
		});
		
		
		
/*function empty()
	{
		var qt = document.cart.qty.value;
		if(qt=="" || isNaN(qt) || (qt%1!=0) || qt==0)
		{
			alert("Please Fill Quantity");
			return false;
		}
		if(qt<=0 || qt>5)
		{
			alert("Maximum 5 product can be entered");
			return false;
		}
	}
function valid() 
		
	{		
		var qt = document.cart.qty.value;
		if(isNaN(qt) || (qt%1!=0) || qt==0)
		{
			alert("Invalid Quantity");
			return false;
		}
		if(qt<=0 || qt>5)
		{
			alert("Maximum 5 product can be entered");
			return false;
		}
		else 
		{
			var pr = document.cart.pr.value;
			var tot = qt*pr;
			document.cart.total.value=tot;
			return true;
		}
	}
		*/
		 
</script>
	
	</body>
</html>












