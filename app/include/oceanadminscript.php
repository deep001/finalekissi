<?php
require_once __DIR__ . '/../../autoload/define.php';
use App\Classes\Config;

?>
<script src="<?php echo Config::path()->JS ;?>/jquery.min.js"></script>
	<script src="<?php echo Config::path()->JS ;?>/angular.min.js"></script>
	<script src="<?php echo Config::path()->JS ;?>/bootstrap.js" type="text/javascript"></script>
	<script src="<?php echo Config::path()->JS ;?>/materialize.min.js" type="text/javascript"></script>
	<script src="<?php echo Config::path()->JS ;?>/custom.js"></script>