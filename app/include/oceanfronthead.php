<?php

require_once __DIR__ . '/../../autoload/define.php';
session_start();
use App\Classes\Config;

?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="shortcut icon" href="https://demo2.madrasthemes.com/vodi-html/favicon.ico">

<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&amp;family=Open+Sans:wght@300;400;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/vendor/font-awesome/css/all.min.css">
<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/vendor/hs-mega-menu/dist/hs-mega-menu.min.css">
<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/vendor/dzsparallaxer/dzsparallaxer.css">
<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/vendor/cubeportfolio/css/cubeportfolio.min.css">
<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/vendor/aos/dist/aos.css">
<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/vendor/slick-carousel/slick/slick.css">
<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/vendor/fancybox/dist/jquery.fancybox.css">

<link rel="stylesheet" href="<?php echo Config::path()->ASSETSFRONT ;?>/css/theme.css">