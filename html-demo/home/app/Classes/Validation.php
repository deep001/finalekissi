<?php 

namespace App\Classes;


class Validation {

    static function validateClientOrderId($cOrderId) {
        if (isset($cOrderId)) {
            if (preg_match("/^[a-f0-9]{32}$/", $cOrderId)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateGoogleAuthCode($code) {
        if (isset($code)) {
            if (preg_match("/^[0-9]{6}$/", $code)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateInt($intVar) {
        if (isset($intVar)) {
            if (preg_match("/^[0-9]+$/", $intVar)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateEmail($userEmail) {
        if (isset($userEmail)) {
            if (filter_var($userEmail, FILTER_VALIDATE_EMAIL, FILTER_FLAG_PATH_REQUIRED)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
	static function validateName($userDetail) {
        if (isset($userDetail)) {
            if (filter_var($userDetail, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	 static function validatenewmail($email) {
        if (isset($email)) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL, FILTER_FLAG_PATH_REQUIRED)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
	static function validateTitle($title) {
        if (isset($title)) {
            if (filter_var($title, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateFirstContent($firstcontent) {
        if (isset($firstcontent)) {
            if (filter_var($firstcontent, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }

		static function validateGalleryTitle($GalleryTitle) {
        if (isset($GalleryTitle)) {
            if (filter_var($GalleryTitle, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateSmallDescription($smalldesc) {
        if (isset($smalldesc)) {
            if (filter_var($smalldesc, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateFullDescription($fulldesc) {
        if (isset($fulldesc)) {
            if (filter_var($fulldesc, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateAuthor($author) {
        if (isset($author)) {
            if (filter_var($author, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateEventHead($Eventheading) {
        if (isset($Eventheading)) {
            if (filter_var($Eventheading, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateEventDescription($Eventdesc) {
        if (isset($Eventdesc)) {
            if (filter_var($Eventdesc, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
		static function validateFname($fname) {
        if (isset($fname)) {
            if (filter_var($fname, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
		
		static function validateMessage($msg) {
        if (isset($msg)) {
            if (filter_var($msg, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
		static function validateSubject($subject) {
        if (isset($subject)) {
            if (filter_var($subject, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validatePhone($phoneno) {
        if (isset($phoneno)) {
            if (filter_var($phoneno, FILTER_SANITIZE_STRING))  {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateLocation($location) {
        if (isset($location)) {
            if (filter_var($location, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
	static function validateCommerceTitle($CommerceTitle) {
        if (isset($CommerceTitle)) {
            if (filter_var($CommerceTitle, FILTER_SANITIZE_STRING)) {
                return TRUE;
            } else  {
				
                return FALSE;
            }
			
        } else {
            return FALSE;
        }
    }
    static function validateUuid($uuid) {
        if (isset($uuid)) {
            if (preg_match("/^[0-9]+$/", $uuid)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /* static function validatePasswordStrength($password) {
        if (isset($password)) {
            if (preg_match("/^[0-9a-zA-Z@!#&%*]{8,50}$/", $password)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }*/
	
	 static function validatePasswordStrength($password) {
        if (isset($password)) {
            if (preg_match("/^[0-9a-zA-Z@!#&%*]{8,50}$/", $password)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

 /*   static function validateOperationId($operationId) {
        if (isset($operationId)) {
            if (preg_match("/^[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}$/", $operationId)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    } */

    static function validateCSRFToken($token) {
        if (isset($token)) {
            if (preg_match("/^[0-9a-z]{32}$/", $token)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateAction($action) {
        if (isset($action)) {
            if (preg_match("/^[a-zA-Z]+$/", $action)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateTradePair($symbol) {
        if (isset($symbol)) {
            if (preg_match("/^[A-Z]+\-[A-Z]+$/", $symbol)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateCoinSymbol($coinSymbol) {
        if (isset($coinSymbol)) {
            if (preg_match("/^[A-Z]+$/", $coinSymbol)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateQty($quantity) {
        if (isset($quantity)) {
            if (preg_match("/^[0-9]*\.?[0-9]{1,8}$/", $quantity)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateRate($rate) {
        if (isset($rate)) {
            if (preg_match("/^[0-9]*\.?[0-9]{1,8}$/", $rate)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateOrderType($orderType) {
        if (isset($orderType)) {
            if (preg_match("/^(buy|sell)+$/", $orderType)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateTradeTypeName($tradeType) {
        if (isset($tradeType)) {
            if (preg_match("/^(MAKER|TAKER)+$/", $tradeType)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateOrderExecuteType($orderExecuteType) {
        if (isset($orderExecuteType)) {
            if (preg_match("/^(market|limit)(\_stop\_loss)?$/", $orderExecuteType)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateOrderExpiry($orderExpiry) {
        if (isset($orderExpiry)) {
            if (preg_match("/^(GTC|IOC)$/", $orderExpiry)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateStopLossFlag($stopLossFlag) {
        if (isset($stopLossFlag)) {
            if (preg_match("/^(Y|N)$/", $stopLossFlag)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateOrderStatus($orderStatus) {
        if (isset($orderStatus)) {
            if (preg_match("/^(" . Order::ORDER_STATUS_ACTIVE . "|" . Order::ORDER_STATUS_CANCELLED . "|" . Order::ORDER_STATUS_FILLED . "|" . Order::ORDER_STATUS_PARTIALLY_FILLED . "|" . Order::ORDER_STATUS_PENDING . ")$/", $orderStatus)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateDateTimeMicro($dataTimeMicro) {
        if (isset($dataTimeMicro)) {
            if (preg_match("/^20\d{1}\d{1}\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[0-1])\ (0\d|1\d|2[0-3])\:[0-5]\d\:[0-5]\d\.\d{3,6}$/", $dataTimeMicro)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateDateTime($dataTime) {
        if (isset($dataTime)) {
            if (preg_match("/^20\d{1}\d{1}\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[0-1])\ (0\d|1\d|2[0-3])\:[0-5]\d\:[0-5]\d\$/", $dataTime)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateEmailWithdrawConfimCode($code) {
        if (isset($code)) {
            if (preg_match("/^[0-9a-f]{32}$/", $code)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateReferralId($referralId) {
        if (isset($referralId)) {
            if (preg_match("/^[0-9a-f]{13}$/", $referralId)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateOrderQty($qty, $decimal) {
        if (isset($qty) && isset($decimal)) {
            if ((string) $decimal > "0") {
                $dotNumber = "1";
                $afterDotNumber = (string) $decimal;
            } else {
                $dotNumber = "0";
                $afterDotNumber = "0";
            }
            if (preg_match("/^[\d]+[.]{" . $dotNumber . "}[\d]{" . $afterDotNumber . "}$/", $qty)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateOrderRate($rate, $decimal) {
        if (isset($rate) && isset($decimal)) {
            if ((string) $decimal > "0") {
                $dotNumber = "1";
                $afterDotNumber = (string) $decimal;
            } else {
                $dotNumber = "0";
                $afterDotNumber = "0";
            }
            if (preg_match("/^[\d]+[.]{" . $dotNumber . "}[\d]{" . $afterDotNumber . "}$/", $rate)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    static function validateSessionId($sessionId) {
        if (isset($sessionId)) {
            if (preg_match("/^[0-9a-v]+$/", $sessionId)) {
                return TRUE;
            } else {
                setcookie(session_name(), '', time() - 7000000, '/');
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}

