<?php 

namespace App\Classes;
use App\Traits\Databasetraits;


class Mail
{
	

    use Databasetraits;
  
     protected $to = "Deepaknautiyal52@gmail.com";
     protected $subject = "Welcome to Tapon! A new inquiry has been sent! You'll get the call from executive shortly";
	
	public function addressDetail()
	{
		   $this->db->query("select * from contactinfo order by contactid ASC");
           $exe =  $this->db->execute();
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
	public function addressDetailadmin()
	{
		   $this->db->query("select * from contactinfo order by contactid ASC");
           $exe =  $this->db->execute();
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	
	public function sentMail($fname,$email,$subject,$msg)
	{
			if (isset($fname)  && isset($email)  && isset($subject)  && isset($msg)) {
				 $fname = trim(strtolower(filter_var($fname, FILTER_SANITIZE_STRING)));
				$email = trim(strtolower(filter_var($email, FILTER_SANITIZE_EMAIL)));
				 $subject = trim(strtolower(filter_var($subject, FILTER_SANITIZE_STRING)));
				$msg = trim(strtolower(filter_var($msg, FILTER_SANITIZE_STRING)));
		
			 if (!Validation::validateFname($fname)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid First Name"
                ];
              
            }
			else if (!Validation::validatenewmail($email)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Email"
                ];
              
            }
				else if (!Validation::validateSubject($subject)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Subject"
                ];
              
            }
				else if (!Validation::validateMessage($msg)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Message"
                ];
              
            }
				else
				{
					 $msg = '
        <html>
        
         <body bgcolor="#DCEEFC">
          <div style="width=600px; border=2px; border:solid;">
           
           <img src="http://rankadmin.com/assets/images/logo.png"  width="50%" height="100px" alt="Countrylink" title="Countrylink" />	
           
          <br />
           <h1>Hello <strong>Tapon</strong>,</h1>
           <br /> 
                       
           <p>A new inquiry has been placed successfully placed. See the detail mention in below:</p>
           
           <table>
           
           <tbody>
           <tr>
           <td width="200px">Customer name: '.$fname.'</td></tr>
           <td width="200px">Registered Email id: '.$email.'</td></tr>
           <tr><td>Subject: '.$subject.' </td></tr>
           <tr><td>Message: '.$msg.' </td></tr>
       
           </tbody>
           </table><br><br>
      
       
       <p>Thank you and enjoy TAPON services!</p>
       <p>All the best,</p>
       <p>Team TAPON</p>
       
      </div>
      
   
   
   
   
     </body> 
   </html> 
    ';	
					
					
					$from = $email;
	
		$headers2 = "From: " . strip_tags($from) . "\r\n";
		$headers2 .= "MIME-Version: 1.0\r\n";
		$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		
		mail($this->to,$this->subject,$msg,$headers2);
if(mail)
{
    
    
    return (object) [
        'status' => true,
        'msg' => 'Congratulations ,You have successfully placed an inquiry with us on Tapon, Now wait for call from one of our executive. ',
    ];
    
 //    Headers::redirect("/contact-us");
}
				}
				
					
			}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid Firstname or email or subject ! Please try again",
                ];
                }
	}
	
	public function getContactDetail($contactid)
	{
		$this->db->query("select * from contactinfo WHERE contactid=:CID");
		$exe = $this->db->execute(array(
		':CID' => $contactid,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	}
	
	public function updateMailDetail($contactid,$phoneno,$emailid,$location)
	{
		if (isset($contactid) && isset($phoneno)  && isset($emailid)  && isset($location)) {
              $phoneno = trim(strtolower(filter_var($phoneno, FILTER_SANITIZE_STRING)));
			 $location = trim(strtolower(filter_var($location, FILTER_SANITIZE_STRING)));
			 $email = trim(strtolower(filter_var($emailid, FILTER_SANITIZE_EMAIL)));
        
        if (!Validation::validatePhone($phoneno)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Phone Number"
                ];
              
            }
			else if (!Validation::validateLocation($location)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Location"
                ];
              
            }
	/*	else if (!Validation::validateEmail($email)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Email id"
                ];
              
            } */
		
		 else {   
				
			

  
				
		
             $this->db->query("UPDATE `contactinfo` set `phoneno`=:PHONE,`emailid`=:EMAIL,`location`=:LOCAT WHERE `contactid`=:CID");
				$update = $this->db->execute(array(
                    ':CID' => $contactid,
					 ':PHONE' => $phoneno,
					':EMAIL' => $emailid,
					':LOCAT' => $location
					
                ));
			
				 if($update)
			  {
					
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'eventreturnid' => $contactid,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
          }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid content ! please try again"
                ];
                }
	}	
	
	
}