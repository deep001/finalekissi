<?php 

namespace App\Classes;
use App\Traits\Databasetraits;

session_start();

class Login
{
	
	
	 protected $from = "deepaknautiyal52@gmail.com";
     //protected $subject = "Welcome to Vedant Darshan! A new user has been placed! please check your admin dshboard";
	 protected $sub = "Congratulations! You have been successfully registered. Please verify your account";
	 
	// protected $useremail = "ndeepak48@gmail.com";
   // protected $last = "Nautiyal";

   // const LOGIN_STATUS_PENDING = "PENDING";
    // const LOGIN_STATUS_SUCCESS = "SUCCESS";
    // const LOGIN_STATUS_FAILED = "FAILED";
	
//	protected $userid = $_SESSION['uid'];
//	protected $useremail = $_SESSION['u_email'];

    use Databasetraits;
  
   /* public function getUserDetail($useremail)
	{
		$this->db->query("select * from admin WHERE email_id=:EMAIL");
		$exe = $this->db->execute(array(
		':EMAIL' => $useremail,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	} */
	/* public function getUserDetailFirst($userid)
	{
		$this->db->query("select * from admin WHERE id=:UID");
		$exe = $this->db->execute(array(
		':UID' => $userid,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	}
	
	
     */



    function getSignInRequest($userEmail, $userPassword)
    {
        if (isset($userEmail) && isset($userPassword)) {
			    $status = 'Active';
                $userEmail = trim(strtolower(filter_var($userEmail, FILTER_SANITIZE_EMAIL)));
                $userPassword = trim(filter_var($userPassword, FILTER_SANITIZE_STRING));
			   // $role = trim(filter_var($role, FILTER_SANITIZE_STRING));
          //  $gRecaptchaResponse = trim(filter_var($gRecaptchaResponse, FILTER_SANITIZE_STRING));

       /*     $googleObj = new Googlelib();
            if (!$googleObj->gRecaptchaValidate($gRecaptchaResponse)) {
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid googleRecaptchaValidate recaptcha, Please try again"
                ];
                  
            } */
				 if (!Validation::validateEmail($userEmail)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid email"
                ];
              
            } 
		/*	else if (!Validation::validatePasswordStrength($userPassword)) {

                return (object)[
                    'status'=>false,
                    'msg'=>"invalid password strength, it can contain atleast 1 upper and 1 lower alphabet, 1 number, 1 special character @!#&%*"
                ];
              
            }  */
			/*else if (!$this->verifyEmailnPassword($userEmail, $userPassword)) {
             
                return (object)[
                    'status'=>false,
                    'msg'=>"incorrect email or password"
                ];
              
            }*/
			else { 
				
			
                $finalp = base64_encode($userPassword);
                $this->db->query("select USERNAME,LOGINID,EMAILID,USER_TYPE from loginone where EMAILID=:EMAIL and PASSWORD=:PASSWORD and STATUS=:STAT"); 
                 $this->db->execute(array(
                    ":EMAIL" => $userEmail,
					 ":PASSWORD" => $finalp,
					 ":STAT" => $status,
                   
                ));
                if ($this->db->rowCount() > 0) {
                    $row = $this->db->fetch();
                  return (object)[
                        'status'=>true,
                        'email'=>$row->EMAILID,
                        'id'=>$row->LOGINID,
					    'role'=>$row->USER_TYPE,
					    'username'=>$row->USERNAME,

                    ];  
				//	return $row;
					
                    
                }else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid email id or password or user not activated yet ! please try again"
                ];
                }
            } 
        } else {
           return Errorlist::errorResponse(false, "incorrect email or password");
        }
    } 

	 public function getUserDetail($sessemailid)
	  { 
	      
	    
   
 
	
      
     $this->db->query("select * from login where EMAILID=:EMAIL");
   $exe =  $this->db->execute(array(
        ":EMAIL" => $sessemailid
         
    ));
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();

   
   return $row;
     
	 }
	 
} 
	
	public function getRole()
	{
		 echo '<option value="Student">Student</option>
			   <option value="School">School</option>';
	}
	
		 public function addUserDetail($userrole,$username,$email,$userpassword)
	{
			if (isset($userrole)  && isset($username)  && isset($email)  && isset($userpassword)) {
				 $userrole = trim(filter_var($userrole, FILTER_SANITIZE_STRING));
				 $username = trim(strtolower(filter_var($username, FILTER_SANITIZE_STRING)));
		         $email = trim(strtolower(filter_var($email, FILTER_VALIDATE_EMAIL)));
				$userpassword = trim(filter_var($userpassword, FILTER_SANITIZE_STRING));
				$status = 'Active';
				//$systemgenpassword = "Rubico".rand();
				
				
			 if (!Validation::validateName($username)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Input"
                ];
              
            }
		
			else if (!Validation::validateEmail($email)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Email"
                ];
              
            }
				/*else if (!Validation::validatePasswordStrength($userpassword)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid password strength, it can contain atleast 1 upper and 1 lower alphabet, 1 number, 1 special character @!#&%*"
                ];
              
            } */
				
				else
				{
				   $finalpassword = base64_encode($userpassword);
				
			    $this->db->query("INSERT INTO `loginone`(`CONTACT_PERSON`,`USERNAME`,`USER_TYPE`,`EMAILID`,`PASSWORD`,`STATUS`) VALUES (:USERNAME,:USERNAME,:USERTYPE,:EMAIL,:PASSWO,:STAT)");
			    
			
					
				$insert = $this->db->execute(array(
                    ":USERNAME" => $username,
					":USERTYPE" => $userrole,
					":EMAIL" => $email,
					":PASSWO" => $finalpassword,
					":STAT" => $status,
					
                ));
			
				 if($insert)
			  {  

					
				    return (object)[
                    'status'=>true,
                    'msg'=>"Successfully Added to Registered User List. Please login",	
                    
                    
                ];
			  
				 }
					 
					 
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Addition ! Please try again"
                ];
                }
				}
				
					
			}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid Detail ! please try again",
                ];
                }
	}
	
	
	
	public function updateUserRecord($id,$Name, $username, $Email, $usertype, $file_name1, $file_tmp1)
	{
		if (isset($id) && isset($Name) && isset($username) && isset($Email) && isset($usertype) && isset($file_name1)) {
               $Name = trim(strtolower(filter_var($Name, FILTER_SANITIZE_STRING)));
                $username = trim(filter_var($username, FILTER_SANITIZE_STRING));
			   $Email = trim(filter_var($Email, FILTER_VALIDATE_EMAIL));
			 $usertype = trim(strtolower(filter_var($usertype, FILTER_SANITIZE_STRING)));
                $file_name1 = trim(filter_var($file_name1, FILTER_SANITIZE_STRING));
			
		 if (!Validation::validateName($Name)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Input"
                ];
              
            }
			else if (!Validation::validateName($username)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Input"
                ];
              
            }
			else if (!Validation::validateEmail($Email)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Email"
                ];
              
            }
				else if (!Validation::validateName($usertype)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Input"
                ];
              
            }
		else {  
				
			

            
				
		
             $this->db->query("UPDATE `admin` set `name`=:NAME,`username`=:USERNAME,`email_id`=:EMAIL, `user_type`=:USERTYPE, `profileimage`=:PROFILEIMAGE WHERE `id`=:UID");
				$update = $this->db->execute(array(
                    ':NAME' => $Name,
					 ':USERNAME' => $username,
					 ':EMAIL' => $Email,
					 ':USERTYPE' => $usertype,
					 ':PROFILEIMAGE' => $file_name1,
					 ':UID' => $id,
                ));
			
				 if($update)
			  {
					 move_uploaded_file($file_tmp1,"images/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'userreturnid' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid content or image ! please try again"
                ];
                }
	}

	public function eventInsert($post)
	{
		
		
		 $firstname = addslashes(trim(strtolower(filter_var($post['first_name'], FILTER_SANITIZE_STRING))));
		  $lastname = addslashes(trim(strtolower(filter_var($post['last_name'], FILTER_SANITIZE_STRING))));
		  $listname = addslashes(trim(strtolower(filter_var($post['list_name'], FILTER_SANITIZE_STRING))));
		  $publishdate = date("Y-m-d",strtotime($post['date']));
		  $listphone = addslashes(trim(strtolower(filter_var($post['list_phone'], FILTER_SANITIZE_STRING))));
		 $email = addslashes(trim(strtolower(filter_var($post['email'], FILTER_VALIDATE_EMAIL))));
		  $listaddr = addslashes(trim(strtolower(filter_var($post['list_addr'], FILTER_SANITIZE_STRING))));
		 $listtype = addslashes(trim(strtolower(filter_var($post['listtype'], FILTER_SANITIZE_STRING))));
		  $city = addslashes(trim(strtolower(filter_var($post['city'], FILTER_SANITIZE_STRING))));
		 $category = $post['category'];
		 $multiplecategory = implode(",",$category);
		
		 
		  $opendays = $post['opendays'];
		  $multipleopendays = implode(",",$opendays);
		  $opentime = addslashes(trim(strtolower(filter_var($post['opentime'], FILTER_SANITIZE_STRING))));
		  $closetime = addslashes(trim(strtolower(filter_var($post['closetime'], FILTER_SANITIZE_STRING))));
		  $listingdescription = addslashes(trim(strtolower(filter_var($post['listingdescription'], FILTER_SANITIZE_STRING))));
		  $fbpageurl = addslashes(trim(strtolower(filter_var($post['fbpageurl'], FILTER_SANITIZE_STRING))));
		  $googlepluspageurl = addslashes(trim(strtolower(filter_var($post['googlepluspageurl'], FILTER_SANITIZE_STRING))));
		  $twitterpageurl = addslashes(trim(strtolower(filter_var($post['twitterpageurl'], FILTER_SANITIZE_STRING))));
		  $serviceguarantee = addslashes(trim(strtolower(filter_var($post['serviceguarantee'], FILTER_SANITIZE_STRING))));
		  $professional = addslashes(trim(strtolower(filter_var($post['professional'], FILTER_SANITIZE_STRING))));
		  $insurance = addslashes(trim(strtolower(filter_var($post['insurance'], FILTER_SANITIZE_STRING))));
		  $googlmap = addslashes(trim(strtolower(filter_var($post['googlmap'], FILTER_SANITIZE_STRING))));
		  $degree = addslashes(trim(strtolower(filter_var($post['360degree'], FILTER_SANITIZE_STRING))));
		  $roombooking = addslashes(trim(strtolower(filter_var($post['roombooking'], FILTER_SANITIZE_STRING))));
		  $servicename = addslashes(trim(strtolower(filter_var($post['servicename'], FILTER_SANITIZE_STRING))));
		 
		 $servicename2 = addslashes(trim(strtolower(filter_var($post['servicename2'], FILTER_SANITIZE_STRING))));
		  $servicename3 = addslashes(trim(strtolower(filter_var($post['servicename3'], FILTER_SANITIZE_STRING))));
		  $servicename4 = addslashes(trim(strtolower(filter_var($post['servicename4'], FILTER_SANITIZE_STRING))));
		  $servicename5 = addslashes(trim(strtolower(filter_var($post['servicename5'], FILTER_SANITIZE_STRING))));
		  $coverimage 			     = $_FILES['coverimage']['name'];
	      $coverimageTemp 		     = $_FILES['coverimage']['tmp_name'];
	      $coverimagetype             = $_FILES['coverimage']['type'];
	      $coverimagesize             = $_FILES['coverimage']['size'];
		  $photogalleryimage			 = $_FILES['photogalleryimage']['name'];
	      $photogalleryimageTemp 	 = $_FILES['photogalleryimage']['tmp_name'];
	      $photogalleryimagetype      = $_FILES['photogalleryimage']['type'];
	      $photogalleryimagesize      = $_FILES['photogalleryimage']['size'];
		  $roombookingphotoimage	     = $_FILES['roombookingphoto']['name'];
	      $roombookingphotoTemp 	     = $_FILES['roombookingphoto']['tmp_name'];
	      $roombookingphototype       = $_FILES['roombookingphoto']['type'];
	      $roombookingphotosize       = $_FILES['roombookingphoto']['size'];
		  $servicenamephotoimage	     = $_FILES['servicenamephoto']['name'];
	      $servicenamephotoTemp 	     = $_FILES['servicenamephoto']['tmp_name'];
	      $servicenamephototype       = $_FILES['servicenamephoto']['type'];
	      $servicenamephotosize       = $_FILES['servicenamephoto']['size'];
		  $servicenamephotoimage2	 = $_FILES['servicenamephoto2']['name'];
	      $servicenamephotoTemp2 	 = $_FILES['servicenamephoto2']['tmp_name'];
	      $servicenamephototype2      = $_FILES['servicenamephoto2']['type'];
	      $servicenamephotosize2      = $_FILES['servicenamephoto2']['size'];
		  $servicenamephotoimage3	 = $_FILES['servicenamephoto3']['name'];
	      $servicenamephotoTemp3 	 = $_FILES['servicenamephoto3']['tmp_name'];
	      $servicenamephototype3      = $_FILES['servicenamephoto3']['type'];
	      $servicenamephotosize3      = $_FILES['servicenamephoto3']['size'];
		  $servicenamephotoimage4	 = $_FILES['servicenamephoto4']['name'];
	      $servicenamephotoTemp4 	 = $_FILES['servicenamephoto4']['tmp_name'];
	      $servicenamephototype4      = $_FILES['servicenamephoto4']['type'];
	      $servicenamephotosize4      = $_FILES['servicenamephoto4']['size'];
		  $servicenamephotoimage5	 = $_FILES['servicenamephoto5']['name'];
	      $servicenamephotoTemp5 	 = $_FILES['servicenamephoto5']['tmp_name'];
	      $servicenamephototype5      = $_FILES['servicenamephoto5']['type'];
	      $servicenamephotosize5      = $_FILES['servicenamephoto5']['size'];
		  $randomNumber               = "ID".rand();
		  $coverimageRandom           = $randomNumber.$coverimage;
	      $photogalleryimageRandom   = $randomNumber.$photogalleryimage;
          $roombookingphotoimageRandom     = $randomNumber.$roombookingphotoimage;
	      $servicenamephotoimageRandom     = $randomNumber.$servicenamephotoimage;
	      $servicenamephotoimagesecondRandom    = $randomNumber.$servicenamephotoimage2;
	      $servicenamephotoimagethirdRandom   = $randomNumber.$servicenamephotoimage3;
          $servicenamephotoimagefourRandom    = $randomNumber.$servicenamephotoimage4;
	      $servicenamephotoimagefiveRandom   = $randomNumber.$servicenamephotoimage5; 
	     
		
		//echo "INSERT INTO `eventlist`(`userid`,`useremail`,`firstname`,`lastname`,`listname`,`listphone`,`eventemail`,`listaddr`,`listtype`,`city`,`category`,`opendays`,`opentime`,`closetime`,`listingdescription`,`fbpageurl`,`googlepluspageurl`,`twitterpageurl`,`serviceguarantee`,`professional`,`insurance`,`googlmap`,`degree`,`roombooking`,`servicename`,`servicenamesecond`,`servicenamethird`,`servicenamefourth`,`servicenamefifth`,`coverimage`,`photogalleryimage`,`roombookingphotoimage`,`servicenamephotoimage`,`servicenamephotoimagesecond`,`servicenamephotoimagethird`,`servicenamephotoimagefourth`,`servicenamephotoimagefifth`) VALUES (:USERID,:USEREMAIL,:FIRSTNAME,:LASTNAME,:LISTNAME,:LISTPHONE,:EVENTEMAIL,:LISTADDR,:LISTTYPE,:CITY,:CATEGORY,:OPENDAYS,:OPENTIME,:CLOSETIME,:DESCRIPTION,:FBPAGEURL,:GOOGLEPLUS,:TWITTER,:SERVICEGUARANTEE,:PROFESSIONAL,:INSURANCE,:GOOGLEMAP,:DEGREE,:ROOMBOOKING,:SERVICENAME,:SERVICENAMESECOND,:SERVICENAMETHIRD,:SERVICENAMEFOURTH,:SERVICENAMEFIFTH,:COVERIMAGE,:PHOTOGALLERYIMAGE,:ROOMBOOKINGPHOTO,:SERVICENAMEPHOTO,:SERVICENAMEPHOTOSECOND,:SERVICENAMEPHOTOTHIRD,:SERVICENAMEPHOTOFOURTH,:SERVICENAMEPHOTOFIFTH)"; die;
		 $this->db->query("INSERT INTO `eventlist`(`userid`,`useremail`,`firstname`,`lastname`,`eventadddate`,`listname`,`listphone`,`eventemail`,`listaddr`,`listtype`,`city`,`category`,`opendays`,`opentime`,`closetime`,`listingdescription`,`fbpageurl`,`googlepluspageurl`,`twitterpageurl`,`serviceguarantee`,`professional`,`insurance`,`googlmap`,`degree`,`roombooking`,`servicename`,`servicenamesecond`,`servicenamethird`,`servicenamefourth`,`servicenamefifth`,`coverimage`,`photogalleryimage`,`roombookingphotoimage`,`servicenamephotoimage`,`servicenamephotoimagesecond`,`servicenamephotoimagethird`,`servicenamephotoimagefourth`,`servicenamephotoimagefifth`) VALUES (:USERID,:USEREMAIL,:FIRSTNAME,:LASTNAME,:PUBLISHDATE,:LISTNAME,:LISTPHONE,:EVENTEMAIL,:LISTADDR,:LISTTYPE,:CITY,:CATEGORY,:OPENDAYS,:OPENTIME,:CLOSETIME,:DESCRIPTION,:FBPAGEURL,:GOOGLEPLUS,:TWITTER,:SERVICEGUARANTEE,:PROFESSIONAL,:INSURANCE,:GOOGLEMAP,:DEGREE,:ROOMBOOKING,:SERVICENAME,:SERVICENAMESECOND,:SERVICENAMETHIRD,:SERVICENAMEFOURTH,:SERVICENAMEFIFTH,:COVERIMAGE,:PHOTOGALLERYIMAGE,:ROOMBOOKINGPHOTO,:SERVICENAMEPHOTO,:SERVICENAMEPHOTOSECOND,:SERVICENAMEPHOTOTHIRD,:SERVICENAMEPHOTOFOURTH,:SERVICENAMEPHOTOFIFTH)");
					
				$insert = $this->db->execute(array(
                    ":USERID" => $_SESSION['uid'],
					":USEREMAIL" => $_SESSION['u_email'],
					":FIRSTNAME" => $firstname,
					":LASTNAME" => $lastname,
					":PUBLISHDATE" => $publishdate,
					":LISTNAME" => $listname,
					":LISTPHONE" => $listphone,
					":EVENTEMAIL" => $email,
					":LISTADDR"   => $listaddr,
					":LISTTYPE" => $listtype,
					":CITY" => $city,
					":CATEGORY" => $multiplecategory,
					":OPENDAYS" => $multipleopendays,
					":OPENTIME" => $opentime,
					":CLOSETIME" => $closetime,
					":DESCRIPTION" => $listingdescription,
					":FBPAGEURL" => $fbpageurl,
					":GOOGLEPLUS" => $googlepluspageurl,
					":TWITTER"           => $twitterpageurl,
					":SERVICEGUARANTEE" => $serviceguarantee,
					":PROFESSIONAL"     => $professional,
					":INSURANCE" => $insurance,
					":GOOGLEMAP" => $googlmap,
					":DEGREE" => $degree,
					":ROOMBOOKING" => $roombooking,
					":SERVICENAME" => $servicename,
					":SERVICENAMESECOND" => $servicename2,
					":SERVICENAMETHIRD" => $servicename3,
					":SERVICENAMEFOURTH" => $servicename4,
					":SERVICENAMEFIFTH" => $servicename5,
					":COVERIMAGE" => $coverimageRandom,
					":PHOTOGALLERYIMAGE" => $photogalleryimageRandom,
					":ROOMBOOKINGPHOTO" => $roombookingphotoimageRandom,
					":SERVICENAMEPHOTO" => $servicenamephotoimageRandom,
					":SERVICENAMEPHOTOSECOND" => $servicenamephotoimagesecondRandom,
					":SERVICENAMEPHOTOTHIRD" => $servicenamephotoimagethirdRandom,
					":SERVICENAMEPHOTOFOURTH" => $servicenamephotoimagefourRandom,
					":SERVICENAMEPHOTOFIFTH" => $servicenamephotoimagefiveRandom
					));
			
				 if($insert)
			  {
					// $_SESSION['URL'] = "hotelmanorama/includes/../attachment";
			         // $path = '/var/www/html/' . $_SESSION['URL'] .'/aadhar/';
					/*$aadarcardpath = $path.'aadhar/';
					  $voteridpath   = $path.'voterid/';
					  $pencardpath   = $path.'pencard/';
					  $profilepath   = $path.'profilephoto/'; */
					  
					  
					 
				$pathfirst = 'images/list-deta/';
				$photogallerypath = 'images/slider/';
				$servicepath = 'images/services/';
			
			//	 $path = 'eventimages/';
					  
				      move_uploaded_file($coverimageTemp,$pathfirst.$coverimageRandom);
					  move_uploaded_file($photogalleryimageTemp,$photogallerypath.$photogalleryimageRandom);
					  move_uploaded_file($roombookingphotoTemp,$servicepath.$roombookingphotoimageRandom);
					  move_uploaded_file($servicenamephotoTemp,$servicepath.$servicenamephotoimageRandom); 
					  move_uploaded_file($servicenamephotoTemp2,$servicepath.$servicenamephotoimagesecondRandom);
					  move_uploaded_file($servicenamephotoTemp3,$servicepath.$servicenamephotoimagethirdRandom);
					  move_uploaded_file($servicenamephotoTemp4,$servicepath.$servicenamephotoimagefourRandom);
					  move_uploaded_file($servicenamephotoTemp5,$servicepath.$servicenamephotoimagefiveRandom);  
					  
					    
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Added to Event list",	
                    
                    
                ];
			  
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Addition ! Please try again"
                ];
                }
		
        
		
	}
	public function getOpenTimeSlots()
	{
		                                    echo '<option value="'.strtotime("24:00").'">12:00 AM</option>
											<option value="'.strtotime("01:00").'">01:00 AM</option>
											<option value="'.strtotime("02:00").'">02:00 AM</option>
											<option value="'.strtotime("03:00").'">03:00 AM</option>
											<option value="'.strtotime("04:00").'">04:00 AM</option>
											<option value="'.strtotime("05:00").'">05:00 AM</option>
											<option value="'.strtotime("06:00").'">06:00 AM</option>
											<option value="'.strtotime("07:00").'">07:00 AM</option>
											<option value="'.strtotime("08:00").'">08:00 AM</option>
											<option value="'.strtotime("09:00").'">09:00 AM</option>
											<option value="'.strtotime("10:00").'">10:00 AM</option>
											<option value="'.strtotime("11:00").'">11:00 AM</option>
											<option value="'.strtotime("12:00").'">12:00 PM</option>
											<option value="'.strtotime("13:00").'">01:00 PM</option>
											<option value="'.strtotime("14:00").'">02:00 PM</option>
											<option value="'.strtotime("15:00").'">03:00 PM</option>
											<option value="'.strtotime("16:00").'">04:00 PM</option>
											<option value="'.strtotime("17:00").'">05:00 PM</option>
											<option value="'.strtotime("18:00").'">06:00 PM</option>
											<option value="'.strtotime("19:00").'">07:00 PM</option>
											<option value="'.strtotime("20:00").'">08:00 PM</option>
											<option value="'.strtotime("21:00").'">09:00 PM</option>
											<option value="'.strtotime("22:00").'">10:00 PM</option>
											<option value="'.strtotime("23:00").'">11:00 PM</option>';
	}
	
	function getAdminSignInRequest($userEmail, $userPassword)
    {
		if (isset($userEmail) && isset($userPassword)) {
              
			 $Email = trim(filter_var($userEmail, FILTER_VALIDATE_EMAIL));
			// $userPassword = trim(strtolower(filter_var($userPassword, FILTER_SANITIZE_STRING)));
				 $userPassword = trim(filter_var($userPassword, FILTER_SANITIZE_STRING));
			 $role = 'superadmin';
              if (!Validation::validateEmail($Email)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid email"
                ];
              
            } else if (!Validation::validatePasswordStrength($userPassword)) {

                return (object)[
                    'status'=>false,
                    'msg'=>"invalid password strength, it can contain atleast 1 upper and 1 lower alphabet, 1 number, 1 special character @!#&%*"
                ];
              
            }
				else { 
				
			
                //$finalp = base64_encode($userPassword);
                $this->db->query("select id,emailid,username from admin where emailid=:EMAIL and password=:PASSWORD and usertype=:ROLE"); 
                 $this->db->execute(array(
                    ":EMAIL" => $Email,
					 ":PASSWORD" => $userPassword,
					 ":ROLE" => $role,
					 
                   
                ));
                if ($this->db->rowCount() > 0) {
                    $row = $this->db->fetch();
                  return (object)[
                        'status'=>true,
                        'email'=>$row->emailid,
                        'id'=>$row->id,
					    'username'=>$row->username,

                    ];  
				//	return $row;
					
                    
                }else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid email id or password or user not activated yet ! please try again"
                ];
                }
            }
		}
		else {
           return Errorlist::errorResponse(false, "incorrect email or password");
        }
	}
	
	public function changeUserPassword($oldpass,$newpass,$email)
	{
		$oldpass = trim(filter_var($oldpass, FILTER_SANITIZE_STRING));
		$newpass = trim(filter_var($newpass, FILTER_SANITIZE_STRING));
		$email = trim(filter_var($email, FILTER_VALIDATE_EMAIL));
		
		 $this->db->query("select PASSWORD from loginone where EMAILID=:EMAIL"); 
                 $this->db->execute(array(
                     ":EMAIL" => $email,
				));
		 if ($this->db->rowCount() > 0) {
                    $row = $this->db->fetch();
                    $oldpassword = $row->PASSWORD;
			        $olddecodepassword = base64_decode($oldpassword);
			        
					if($olddecodepassword != $oldpass)
					{
						 return (object)[
                         'status'=>false,
                         'msg'=>"Please enter only registered current password"
                ];
					}
			  else
			  {  
				  $newencodepass = base64_encode($newpass);
				 $this->db->query("UPDATE `loginone` set `PASSWORD`=:PASS WHERE `EMAILID`=:EMAIL");
				$update = $this->db->execute(array(
                    ':PASS' => $newencodepass,
					':EMAIL' => $email,
					
                )); 
				  
				   if($update)
			  {
					// move_uploaded_file($file_tmp1,"images/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Change the Password",	
                    'userreturnid' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
				  
				  
				  
			  }
			 
			 
                    
                }
		
	}
}