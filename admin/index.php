<?php
require_once __DIR__ . '/../autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\ChannelLogin;
use App\Classes\Csrf;
use App\Classes\Headers;

session_start();
if(isset($_POST["login"]))
{
	 $validationcsrf = new stdClass();
	if(isset($_POST["Email"]) && isset($_POST['password']) && isset($_POST['csrf-token']))
	{
		         // $userEmail = strtolower(filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_EMAIL));
		         $userEmail = filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_EMAIL);
              $userPassword = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING); 
        
              // $gRecaptchaResponse = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_SANITIZE_STRING);
                  $csrfToken = filter_input(INPUT_POST, 'csrf-token', FILTER_SANITIZE_STRING);
		
		   if ($_SESSION['csrf'] != $csrfToken) {
          

           $validationcsrf->status = "FALSE";
            $validationcsrf->msg = "Invalid token, Please try again!!";
        
         
        }
		else
			{
            $login = new ChannelLogin();
            $signinresponse = $login->getAdminSignInRequest($userEmail, $userPassword);
           if($signinresponse->status == true)
			{
				$_SESSION['u_email'] = $signinresponse->email;
                $_SESSION['uid'] = $signinresponse->id;
			    $_SESSION['username'] = $signinresponse->username;
                unset($_SESSION['csrf']);
                Headers::redirect("/project/ocnew/admin/audio.php"); 
            }
			
			else if ($signinresponse->status == false) {
                $responseerror =  $signinresponse->msg;
                  
            }
			
        }
	}
}

$getcsrf = Csrf::getCsrfToken();
$_SESSION['csrf'] = $getcsrf;

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Admin login - Ocean Tv</title>
	`<?php include_once Config::path()->INCLUDE_PATH.'/oceanadminhead.php'; ?>`
	
</head>

<body data-ng-app="">
	<!--<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>-->
	<!--TOP SEARCH SECTION-->
	<?php include_once Config::path()->INCLUDE_PATH.'/oceannewadminheader.php'; ?>
	<section class="tz-register">
			<div class="log-in-pop">
				<!--<div class="log-in-pop-left">
					<h1>Hello... <span>{{ name1 }}</span></h1>
					<p>Don't have an account? Create your account. It's take less then a minutes</p>
					<h4>Login with social media</h4>
					<ul>
						<li><a href="login.html#"><i class="fa fa-facebook"></i> Facebook</a>
						</li>
						<li><a href="login.html#"><i class="fa fa-google"></i> Google+</a>
						</li>
						<li><a href="login.html#"><i class="fa fa-twitter"></i> Twitter</a>
						</li>
					</ul>
				</div>-->
				<div class="log-in-pop-right" style="align-content: center;">
					<a href="login.html#" class="pop-close" data-dismiss="modal"><img src="http://rn53themes.net/themes/demo/directory/images/cancel.png" alt="" />
					</a>
					<h4>Login</h4>
					<?php
						echo (isset($validationcsrf->msg))? '<div class="alert alert-primary" style="color:red;">'.$validationcsrf->msg.'</div>':'';
						echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
					<!--<p>Don't have an account? Create your account. It's take less then a minutes</p>-->
					<form class="s12" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
						<div>
							<div class="input-field s12">
								<!--<input type="text" data-ng-model="name1" class="validate">-->
								<input type="email" class="validate"  id="Email" name="Email"  required >
								<label>Email</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
							<!--	<input type="password" class="validate"> -->
							<input type="password" class="validate" id="password" name="password" required>
								<label>Password</label>
							</div>
						</div>
						<div>
							<div class="input-field s4">
							<input type="hidden" name="csrf-token" value="<?php echo $_SESSION['csrf']; ?>">
								<input type="submit" value="Login" name="login" id="login" class="waves-effect waves-light log-in-btn"> </div>
						</div>
						<div>
							<div class="input-field s12"> <a href="forgot-pass.html">Forgot password</a>
							<!-- | <a href="register.html">Create a new account</a>-->
							                               </div>
						</div>
					</form>
				</div>
			</div>
	</section>
	<!--MOBILE APP-->
<!--	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="login.html#"><img src="images/android.png" alt="" /> </a>
					<a href="login.html#"><img src="images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> -->
	<!--FOOTER SECTION-->
<!--	<footer id="colophon" class="site-footer clearfix">
		<div id="quaternary" class="sidebar-container " role="complementary">
			<div class="sidebar-inner">
				<div class="widget-area clearfix">
					<div id="azh_widget-2" class="widget widget_azh_widget">
						<div data-section="section">
							<div class="container">
								<div class="row">
									<div class="col-sm-4 col-md-3 foot-logo"> <img src="images/foot-logo.png" alt="logo">
										<p class="hasimg">Worlds's No. 1 Local Business Directory Website.</p>
										<p class="hasimg">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
									</div>
									<div class="col-sm-4 col-md-3">
										<h4>Support & Help</h4>
										<ul class="two-columns">
											<li> <a href="advertise.html">Advertise us</a> </li>
											<li> <a href="about-us.html">About Us</a> </li>
											<li> <a href="customer-reviews.html">Review</a> </li>
											<li> <a href="how-it-work.html">How it works </a> </li>
											<li> <a href="add-listing.html">Add Business</a> </li>
											<li> <a href="login.html#!">Register</a> </li>
											<li> <a href="login.html#!">Login</a> </li>
											<li> <a href="login.html#!">Quick Enquiry</a> </li>
											<li> <a href="login.html#!">Ratings </a> </li>
											<li> <a href="trendings.html">Top Trends</a> </li>
										</ul>
									</div>
									<div class="col-sm-4 col-md-3">
										<h4>Popular Services</h4>
										<ul class="two-columns">
											<li> <a href="login.html#!">Hotels</a> </li>
											<li> <a href="login.html#!">Hospitals</a> </li>
											<li> <a href="login.html#!">Transportation</a> </li>
											<li> <a href="login.html#!">Real Estates </a> </li>
											<li> <a href="login.html#!">Automobiles</a> </li>
											<li> <a href="login.html#!">Resorts</a> </li>
											<li> <a href="login.html#!">Education</a> </li>
											<li> <a href="login.html#!">Sports Events</a> </li>
											<li> <a href="login.html#!">Web Services </a> </li>
											<li> <a href="login.html#!">Skin Care</a> </li>
										</ul>
									</div>
									<div class="col-sm-4 col-md-3">
										<h4>Cities Covered</h4>
										<ul class="two-columns">
											<li> <a href="login.html#!">Atlanta</a> </li>
											<li> <a href="login.html#!">Austin</a> </li>
											<li> <a href="login.html#!">Baltimore</a> </li>
											<li> <a href="login.html#!">Boston </a> </li>
											<li> <a href="login.html#!">Chicago</a> </li>
											<li> <a href="login.html#!">Indianapolis</a> </li>
											<li> <a href="login.html#!">Las Vegas</a> </li>
											<li> <a href="login.html#!">Los Angeles</a> </li>
											<li> <a href="login.html#!">Louisville </a> </li>
											<li> <a href="login.html#!">Houston</a> </li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div data-section="section" class="foot-sec2">
							<div class="container">
								<div class="row">
									<div class="col-sm-3">
										<h4>Payment Options</h4>
										<p class="hasimg"> <img src="images/payment.png" alt="payment"> </p>
									</div>
									<div class="col-sm-4">
										<h4>Address</h4>
										<p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</p>
										<p> <span class="strong">Phone: </span> <span class="highlighted">+01 1245 2541</span> </p>
									</div>
									<div class="col-sm-5 foot-social">
										<h4>Follow with us</h4>
										<p>Join the thousands of other There are many variations of passages of Lorem Ipsum available</p>
										<ul>
											<li><a href="login.html#!"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
											<li><a href="login.html#!"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
											<li><a href="login.html#!"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
											<li><a href="login.html#!"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
											<li><a href="login.html#!"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
											<li><a href="login.html#!"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> </li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
		
	</footer>-->
	<!--COPY RIGHTS-->
	<section class="copy">
		<div class="container">
			<p>copyrights © 2020 Rankmantra. &nbsp;&nbsp;All rights reserved. </p>
		</div>
	</section>
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<?php include_once Config::path()->INCLUDE_PATH.'/oceanadminscript.php';?>
</body>

</html>