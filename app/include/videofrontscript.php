<?php
require_once __DIR__ . '/../../autoload/define.php';
use App\Classes\Config;

?>
<!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
<script src="<?php echo Config::path()->ASSETSFRONT; ?>/js/jquery/jquery-2.2.4.min.js"></script>

   <!-- <script src="<?php  //echo Config::path()->ASSETSFRONT ;?>/js/jquery/jquery.min.js"></script>-->
    <!-- Popper js -->
    <script src="<?php echo Config::path()->ASSETSFRONT; ?>/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo Config::path()->ASSETSFRONT; ?>/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo Config::path()->ASSETSFRONT; ?>/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo Config::path()->ASSETSFRONT; ?>/js/active.js"></script>





   