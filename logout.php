<?php
require_once __DIR__ . '/autoload/define.php';
use App\Classes\Headers;
 session_start();  // session start
    session_unset();  // delete all values from session but session still exists
    session_destroy(); // destroy session itself
  Headers::redirect("/project/ocnew/signin.php");
?>