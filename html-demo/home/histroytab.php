<?php
?>
<section class="bg-gray-1100 dark">
<div class="container px-md-4">
<div class="pt-5 mt-1 mb-8">
<!--<h6 class="font-size-16 font-weight-normal opacity-6 text-lh-md pl-1 mb-3">Featured</h6>
<h5 class="display-10">Discover ’18</h5>-->
<h4 class="font-size-22 font-weight-light pl-1 max-w-330">History TV Episodes</h4>
</div>
<div class="tab-nav__v3 mb-3">
<ul class="nav flex-nowrap flex-lg-wrap justify-content-start overflow-auto align-items-center" role="tablist">
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link active pl-2" id="pills-one-code-features-example7-tab" data-toggle="pill" href="#pills-one-code-features-example7" role="tab" aria-controls="pills-one-code-features-example7" aria-selected="true">New Arrivals</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link" id="pills-two-code-features-example8-tab" data-toggle="pill" href="#pills-two-code-features-example8" role="tab" aria-controls="pills-two-code-features-example8" aria-selected="false">Comedy</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link" id="pills-three-code-features-example9-tab" data-toggle="pill" href="#pills-three-code-features-example9" role="tab" aria-controls="pills-three-code-features-example9" aria-selected="false">Drama</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link" id="pills-four-code-features-example10-tab" data-toggle="pill" href="#pills-four-code-features-example10" role="tab" aria-controls="pills-four-code-features-example10" aria-selected="false">Sci-Fi</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link" id="pills-five-code-features-example11-tab" data-toggle="pill" href="#pills-five-code-features-example11" role="tab" aria-controls="pills-five-code-features-example11" aria-selected="false">Action</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link" id="pills-six-code-features-example12-tab" data-toggle="pill" href="#pills-six-code-features-example12" role="tab" aria-controls="pills-six-code-features-example12" aria-selected="false">Thriller</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link" id="pills-seven-code-features-example13-tab" data-toggle="pill" href="#pills-seven-code-features-example13" role="tab" aria-controls="pills-seven-code-features-example13" aria-selected="false">Horror</a>
</li>
</ul>
</div>
<div class="tab-content u-slick__tab">
<div class="tab-pane fade show active" id="pills-one-code-features-example7" role="tabpanel" aria-labelledby="pills-one-code-features-example7-tab">
<div class="js-slick-carousel u-slick" data-hs-slick-carousel-options='{
                                        "prevArrow": "<span class=\"fas fa-chevron-left slick-arrow slick-arrow-svg-left slick-arrow-centered-y left slick-arrow-right rounded-circle position-absolute left-0 ml-n5 ml-wd-n5\"></span>",
                                        "nextArrow": "<span class=\"fas fa-chevron-right slick-arrow slick-arrow-svg-right slick-arrow-centered-y right slick-arrow-right rounded-circle position-absolute mr-n5 mr-wd-n5\"></span>"
                                        }'>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
 </div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
 <div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
 </div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
 <a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-two-code-features-example8" role="tabpanel" aria-labelledby="pills-two-code-features-example8-tab">
<div class="js-slick-carousel u-slick" data-hs-slick-carousel-options='{
                                        "prevArrow": "<span class=\"fas fa-chevron-left slick-arrow slick-arrow-svg-left slick-arrow-centered-y left slick-arrow-right rounded-circle position-absolute left-0 ml-n5 ml-wd-n5\"></span>",
                                        "nextArrow": "<span class=\"fas fa-chevron-right slick-arrow slick-arrow-svg-right slick-arrow-centered-y right slick-arrow-right rounded-circle position-absolute mr-n5 mr-wd-n5\"></span>"
                                        }'>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
 <span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
 <a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
 <div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
</div>
 </div>
<div class="tab-pane fade" id="pills-three-code-features-example9" role="tabpanel" aria-labelledby="pills-three-code-features-example9-tab">
<div class="js-slick-carousel u-slick" data-hs-slick-carousel-options='{
                                        "prevArrow": "<span class=\"fas fa-chevron-left slick-arrow slick-arrow-svg-left slick-arrow-centered-y left slick-arrow-right rounded-circle position-absolute left-0 ml-n5 ml-wd-n5\"></span>",
                                        "nextArrow": "<span class=\"fas fa-chevron-right slick-arrow slick-arrow-svg-right slick-arrow-centered-y right slick-arrow-right rounded-circle position-absolute mr-n5 mr-wd-n5\"></span>"
                                        }'>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
 <div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
 </div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-four-code-features-example10" role="tabpanel" aria-labelledby="pills-four-code-features-example10-tab">
<div class="js-slick-carousel u-slick" data-hs-slick-carousel-options='{
                                        "prevArrow": "<span class=\"fas fa-chevron-left slick-arrow slick-arrow-svg-left slick-arrow-centered-y left slick-arrow-right rounded-circle position-absolute left-0 ml-n5 ml-wd-n5\"></span>",
                                        "nextArrow": "<span class=\"fas fa-chevron-right slick-arrow slick-arrow-svg-right slick-arrow-centered-y right slick-arrow-right rounded-circle position-absolute mr-n5 mr-wd-n5\"></span>"
                                        }'>
<div class="js-slide">
 <div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
 <div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
 </div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
 </div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-five-code-features-example11" role="tabpanel" aria-labelledby="pills-five-code-features-example11-tab">
<div class="js-slick-carousel u-slick" data-hs-slick-carousel-options='{
                                        "prevArrow": "<span class=\"fas fa-chevron-left slick-arrow slick-arrow-svg-left slick-arrow-centered-y left slick-arrow-right rounded-circle position-absolute left-0 ml-n5 ml-wd-n5\"></span>",
                                        "nextArrow": "<span class=\"fas fa-chevron-right slick-arrow slick-arrow-svg-right slick-arrow-centered-y right slick-arrow-right rounded-circle position-absolute mr-n5 mr-wd-n5\"></span>"
                                        }'>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
 <div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
 <div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
 <div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
 <a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
 </a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-six-code-features-example12" role="tabpanel" aria-labelledby="pills-six-code-features-example12-tab">
<div class="js-slick-carousel u-slick" data-hs-slick-carousel-options='{
                                        "prevArrow": "<span class=\"fas fa-chevron-left slick-arrow slick-arrow-svg-left slick-arrow-centered-y left slick-arrow-right rounded-circle position-absolute left-0 ml-n5 ml-wd-n5\"></span>",
                                        "nextArrow": "<span class=\"fas fa-chevron-right slick-arrow slick-arrow-svg-right slick-arrow-centered-y right slick-arrow-right rounded-circle position-absolute mr-n5 mr-wd-n5\"></span>"
                                        }'>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
 </div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
 <div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
 </a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
 </div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-seven-code-features-example13" role="tabpanel" aria-labelledby="pills-seven-code-features-example13-tab">
<div class="js-slick-carousel u-slick" data-hs-slick-carousel-options='{
                                        "prevArrow": "<span class=\"fas fa-chevron-left slick-arrow slick-arrow-svg-left slick-arrow-centered-y left slick-arrow-right rounded-circle position-absolute left-0 ml-n5 ml-wd-n5\"></span>",
                                        "nextArrow": "<span class=\"fas fa-chevron-right slick-arrow slick-arrow-svg-right slick-arrow-centered-y right slick-arrow-right rounded-circle position-absolute mr-n5 mr-wd-n5\"></span>"
                                        }'>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
 <span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
 <div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
 <div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
 <img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
 <img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
<div class="js-slide">
<div class="row mx-n2">
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img2.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Adventures</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">History</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Spiderman 3</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img3.jpg" alt="Image Description">
</a>
 </div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Drama</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Phantom Thread</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img4.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Made Normal</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img5.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Fantacy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Every Day</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid w-100" src="../../assets/img/122x183/img6.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Love, Simon</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img7.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Tale</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img8.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">Mad</a></div>
</div>
</div>
<div class="col-6 col-md-4 col-lg px-2">
<div class="product mt-2">
<div class="product-image mb-1">
<a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid w-100" src="../../assets/img/122x183/img1.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12">
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">2020</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Action</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Comedy</a></span>
<span><a href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="font-weight-bold font-size-1"><a class="text-dark" href="https://demo2.madrasthemes.com/vodi-html/html-demo/single-movies/single-movies-v6.html">The Lost Viking</a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>