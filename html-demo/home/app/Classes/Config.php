<?php 

namespace App\Classes;

class Config
{

    const env_production = false;

    public static function app() 
    {
        
        return (object)[
            'title' => 'Atriark',
            'description' => 'ATRIARK Cryptocurrency Market and Trading Exchange',
            'currency' => 'USD',
            'timezone' => 'UTC',
            'path_url' => (self::env_production === true) ? 'https://user.demo.atriark.co.uk' : 'admin.atriark.test',
            'origin' => (self::env_production === true) ? 'https://user.demo.atriark.co.uk' : 'admin.atriark.test',
            'path_url_text' => (self::env_production === true) ? 'user.demo.atriark.co.uk' : 'admin.atriark.test',
            'alt_title' => 'atriark',
            'app_name' => 'ATRIARK',
            'copyright' => '© Ellylink Limited - Bitselly. 2018 - Lancashire, United Kingdom',
            'secure' => (self::env_production === true) ? true : false,

        ];
    }


    public static function assetsUrl()
    {
        return (object)[
            'favicon' => "",
            'logo' => 'img/logo-light.png',
            'dark_logo' => 'img/logo-light.png',
            'fb_icon' => 'img/fb.png',
            'twitter_icon' => 'img/twitter.png',
            'instagram_icon' => '',
            'adminavatar' => 'img/user.png',

        ];
    }
    public static function googleAuth()
    {
        if (self::env_production === true) {
            return (object)[
                'issuer_name' => 'Atriark,co,uk',
                'code_length' => 6,
            ];
        } else {
            return (object)[
                'issuer_name' => 'Atriark,co,uk',
                'code_length' => 6,
            ];
        }

    }
    public static function googleRecaptcha()
    {
        if (self::env_production === true) {
            return (object)[
                'google_captcha_key' => '6LeUmEAUAAAAAMylWCHuUlThfySdGdFSklFgC_MX',
                'google_captcha_secret' => '6LeUmEAUAAAAAGdpfZhS_CHEybs8hsWMDp0wJ8ZR',
            ];
        } else {
            return (object)[
                'google_captcha_key' => '6LeUmEAUAAAAAMylWCHuUlThfySdGdFSklFgC_MX',
                'google_captcha_secret' => '6LeUmEAUAAAAAGdpfZhS_CHEybs8hsWMDp0wJ8ZR',
            ];
        }

    }

    public static function socialLinks()
    {
        return (object)[
            'fb' => 'https://www.facebook.com/bitselly/',
            'twitter' => 'https://twitter.com/bitselly',
            'telegram' => 'https://t.me/bitselly',
            'medium' => 'https://medium.com/@bitselly',
            'youtube' => 'https://www.youtube.com/channel/UCoFMhOcr3FK8rY-eYhnmEew',
        ];
    }

    public static function debug()
    {
        return (object)[
            'error_reporting' => 0,
        ];
    }

    public static function db()
    {
        if (self::env_production === true) {
            return (object)[
                'default' => (object)[
                    'db_name' => 'demoatriarkco_atriark_mlm',
                    'db_user' => 'demoatriarkco_atriark_user',
                    'db_password' => 'E&Ka=mz9+DG%',
                    'db_host' => '127.0.0.1',
                    'db_port' => '3306',
                    

                ],
            ];
        } else {
            return (object)[
                'default' => (object)[
                    'db_name' => 'atriark',
                    'db_user' => 'root',
                    'db_password' => 'password',
                    'db_host' => '127.0.0.1',
                    'db_port' => '3306',
                ],
            ];
        }
    }

    public static function sessions()
    {
        return (object)[
            'session_name' => 'SESSID_EX_ADMIN_ATRK',
            'max_session_idle_time' => 14400,
            'cookie_secure' => (self::env_production === true) ? true : false,
            'settings' => (object)[
                'auto_start' => 0,
                'use_cookies' => 1,
                'use_only_cookies' => 1,
                'use_trans_sid' => 0,
                'use_strict_mode' => 1,
                'cookie_httponly' => 1,
                'cookie_domain' => '',
                'cookie_path' => '/',
                'cookie_lifetime' => 28800,
                'cache_limiter' => 'nocache',
                'gc_maxlifetime' => 28800,
                'gc_probability' => 1,
                'gc_divisor' => 100,
                'hash_function' => 1,
                'hash_bits_per_character' => 4,
            ],
        ];
    }

    public static function email()
    {
        if (self::env_production === true) {
            return (object)[
                'default' => (object)[
                    'host' => 'mail.demo.atriark.co.uk',
                    'port' => 587,
                    'from' => array(
                        'do-no-reply@demo.atriark.co.uk' => 'Atriark',
                    ),
                    'username' => 'noreply@bitselly.com',
                    'password' => '1r{jT)?hk8kh',
                ],
                'support_email' => 'do-not-reply@demo.atriark.co.uk',
                'withdrawal_email' => 'withdrawal@bitselly.com',
            ];
        } else {
            return (object)[
                'default' => (object)[
                    'host' => 'smtp.mailtrap.io',
                    'port' => 2525,
                    'from' => array(
                        'from@atriark.co.uk' => 'Atriark',
                    ),
                    'username' => '585c97fdc5030c',
                    'password' => '22a66d8356171c',
                ],
                'support_email' => 'support@atriark.co.uk',
                'withdrawal_email' => 'withdrawal@bitselly.com',
            ];
        }
    }

    public static function socialUsersLinks()
    {
        return (object)[
            'fb' => 'https://www.facebook.com/',
            'twitter' => 'https://twitter.com/',
            'instagram' => 'https://www.instagram.com/',
            'linkedin' => 'https://www.instagram.com/',
        ];
    }
    public static function encryption()
    {
        return (object)[
            'salt' => '563f7e758ce7c91b756c659ac634cdfec1108e760c3347181aa6024b6e16c9c0',
            'coin_keys_path' => APP_PATH . '/keys',
        ];
    }
    static public function cronPath()
    {
        if (self::env_production === true) {
            return (object)[
                'path' => '/home/useratriarkco/www'
            ];
        } else {
            return (object)[
                'path' => '/Users/mohitbatra/www/exchange.atriark.test' . APP_PATH . '/cron'
            ];
        }
    }

    public static function checkStatus()
    {
        return (object)[
            'loginfailed' => 'invalid credentials ! Try again',
            'logout' => 'Logout Successfully done',
            'checkstatus' => 'You need to login first!',
            'token' => 'invalid token !',


        ];
    }

    public static function path()
    {
        return (object)[

            'INCLUDE_PATH' => __DIR__ . '/../include',
            'LIB_PATH' => 'lib',
            'LOGOUT' =>'logout.php',
            'ASSETSFRONT' =>'assets',
            'CSS' =>'css',
            'JS' =>'js',

        ];

    }

}


