<?php
session_start();
require_once __DIR__ . '/../autoload/define.php';
use App\Classes\Config;
use App\Classes\Homepage;
use App\Classes\Audio;
use App\Classes\Headers;


$audio = new Audio();
$getaudiolist = $audio->getAudioRecordCompleteDetail();

if(isset($_GET['audioid']) && !empty($_GET['audioid']))
{
	 $audioid = base64_decode($_GET['audioid']);
	 $getaudiodetail = $audio->getAudioCompleteInfo($audioid);
}

if(isset($_POST["updateaudio"]))
{

		
          
            $updatedetail = new Audio();
             $update = $updatedetail->updateaudiodetail($_POST);
		     
             
		 
          
           if($update->status == true)
			{
				
              
	
			
			 ?>  
			   <script>alert("successfully updated"); window.location.href='/admin/audio.php'</script> 
                
			   
       <?php     }
			
			else if ($update->status == false) { 
                $responseerror =  $update->msg;
				
				
                  
            }
			
        
   
} 



?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>World Best Local Directory Website template</title>
	<?php include_once Config::path()->INCLUDE_PATH.'/adminhead.php'; ?>
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	<?php include_once Config::path()->INCLUDE_PATH.'/adminheader.php'; ?>
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			<?php include_once Config::path()->INCLUDE_PATH.'/adminleftsidebar.php'; ?>
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="/admin/audio.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="/admin/updateaudio.php"> Edit Audio Info</a> </li>
						<li class="page-back"><a href="/admin/audio.php"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>Update Audio Information:</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="admin-list-category-add.html#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
						<ul id="dr-list" class="dropdown-content">
							<li><a href="admin-list-category-add.html#!">Add New</a> </li>
							<li><a href="admin-list-category-add.html#!">Edit</a> </li>
							<li><a href="admin-list-category-add.html#!">Update</a> </li>
							<li class="divider"></li>
							<li><a href="admin-list-category-add.html#!"><i class="material-icons">delete</i>Delete</a> </li>
							<li><a href="admin-list-category-add.html#!"><i class="material-icons">subject</i>View All</a> </li>
							<li><a href="admin-list-category-add.html#!"><i class="material-icons">play_for_work</i>Download</a> </li>
						</ul>
						<!-- Dropdown Structure -->
				<?php		echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':''; ?>
						<div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp ad-mar-to-min">
									<div class="tab-inn ad-tab-inn">
										<div class="tz2-form-pay tz2-form-com">
											<form class="" name="frmaudio" id="frmaudio" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
												<div class="row">
													<div class="input-field col s12">
														<select name = "channelname" id="channelname">
															<!--<option value="" disabled selected>Select </option>-->
															<option value="1">CHANNELONE</option>
															<option value="2">CHANNELTWO</option>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<input type="text" class="validate" name="audioname" id="audioname" value="<?php echo stripslashes($getaudiodetail->audioname);?>" required>
														<input type="hidden" name="audioid" value="<?php echo $audioid;?>">
														<label>Live Name</label>
													</div>
												</div>
												<br/>
												<img src="../images/channelimages/<?php echo $getaudiodetail->audioimage;?>" style="height: 150px;">
												<div class="row tz-file-upload">
													<div class="file-field input-field">
														<div class="tz-up-btn"> <span>Audio Image</span>
															
															<input type="file" name="audioimage" id="audioimage" value="" >
														
														</div>
														<div class="file-path-wrapper">
															<input class="file-path validate" type="text"> </div>
													</div>
												</div>
											
											<!--	<div class="row">
													<div class="input-field col s12">
														<?php //echo stripslashes($getlivedetail->liveiframe);?>
														
													</div>
												</div>-->
										
												<div class="row">
													<div class="input-field col s12">
											<textarea id="textarea1" class="materialize-textarea" name="audiodesc"  required><?php echo stripslashes($getaudiodetail->audiodescription); ?></textarea>
											<label for="textarea1" class="">Live Description....</label>
										</div>
													<div class="row">
													<div class="input-field col s12">
											<textarea id="textarea1" class="materialize-textarea" name="audioiframe"  required><?php echo stripslashes($getaudiodetail->audioiframe); ?></textarea>
											<label for="textarea1" class="">Iframe Code here....</label>
										        </div>
												</div>
									<!--				<div class="row">
														<div class="input-field col s12">
															<textarea id="textarea1" class="" rows="4" cols="50" name="liveiframe"  required><?php //echo $getlivedetail->liveiframe; ?></textarea>
															<label for="textarea1">Iframe Code here....</label>
														</div>
													</div> -->
													<!--<div class="row">
										<div class="db-v2-list-form-inn-tit">
											<h5>Iframe:</h5>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input type="text" class="validate" name="lm" value="<?php //echo $getlivedetail->liveiframe; ?>">
											<label>Past your iframe code here</label>
										</div>
									</div> -->
												<div class="row">
													<div class="input-field col s12">
														<input type="submit" name="updateaudio" id="updateaudio" value="Update Live Tv Detail" class="waves-effect waves-light full-btn"> </div>
													
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<?php include_once Config::path()->INCLUDE_PATH.'/adminscript.php'; ?>
</body>

</html>