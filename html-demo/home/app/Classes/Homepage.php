<?php 

namespace App\Classes;
use App\Traits\Databasetraits;


class Homepage
{
	

    use Databasetraits;
	
	  public function getLogoList()
	  { 
     $this->db->query("select * from logo");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetchAll();
     return $row;
  }
	}
	
	public function getTopBanner()
	  { 
     $this->db->query("select * from hometopbanner");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetchAll();
     return $row;
  }
	}
  
   /*  public function getContentRecordFirst()
	  { 
     $this->db->query("select * from homepagecontentfirst order by id DESC");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();
     return $row;
  }
	}
	
	public function getHomePageContentFirst($id)
	{
		$this->db->query("select * from homepagecontentfirst WHERE id=:ID");
		$exe = $this->db->execute(array(
		':ID' => $id,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	}
	
	public function updateHomePageRecord($id,$title, $firstcontent, $AboutFirstcontent, $AboutSecondcontent, $file_name1, $file_tmp1)
	{
		if (isset($id) && isset($title) && isset($firstcontent) && isset($file_name1)) {
               $title = trim(strtolower(filter_var($title, FILTER_SANITIZE_STRING)));
                $firstcontent = trim(filter_var($firstcontent, FILTER_SANITIZE_STRING));
			   $file_name1 = trim(filter_var($file_name1, FILTER_SANITIZE_STRING));
		 if (!Validation::validateTitle($title)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Title"
                ];
              
            }
		 else if (!Validation::validateFirstContent($firstcontent)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Content"
                ];
              
            }
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `homepagecontentfirst` set `title`=:TITLE,`image`=:IMG,`Content`=:FIRSTCONTENT, `Fullcontent`=:ABOUTFIRST, `FinalFullContent`=:ABOUTFULL WHERE `id`=:ID");
				$update = $this->db->execute(array(
                    ':TITLE' => $title,
					 ':IMG' => $file_name1,
					 ':FIRSTCONTENT' => $firstcontent,
					 ':ABOUTFIRST' => $AboutFirstcontent,
					 ':ABOUTFULL' => $AboutSecondcontent,
					 ':ID' => $id,
                ));
			
				 if($update)
			  {
					 move_uploaded_file( $file_tmp1,"/../assets/images/about/$file_name1");
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'userreturnid' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content or image ! please try again"
                ];
                }
	} */
	
	public function updatelogo($post)
	{
		 $logoname = addslashes(trim(strtolower(filter_var($post['logoname'], FILTER_SANITIZE_STRING))));
		 $logo			     = $_FILES['logo']['name'];
	     $logoTemp 		     = $_FILES['logo']['tmp_name'];
	     $logotype             = $_FILES['logo']['type'];
	     $logosize             = $_FILES['logo']['size'];
	     $id = '1';
		 if (!Validation::validateTitle($logoname)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid logo name"
                ];
              
            }
			else {  
				
			

  
				
		
             $this->db->query("UPDATE `logo` set `logoname`=:LOGONAME,`logoimage`=:IMG WHERE `id`=:ID");
				$update = $this->db->execute(array(
                    ':LOGONAME' => $logoname,
					 ':IMG' => $logo,
					 ':ID' => $id,
                ));
			
				 if($update)
			  {
					 $pathfirst = '../img/';
					  // move_uploaded_file($photogalleryimageTemp,$photogallerypath.$photogalleryimageRandom);
					 move_uploaded_file( $logoTemp,$pathfirst.$logo);
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'id' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
	}
     
	
	public function updateBanner($post)
	{
		 
		 $banner			     = $_FILES['banner']['name'];
	     $bannerTemp 		     = $_FILES['banner']['tmp_name'];
	     $bannertype             = $_FILES['banner']['type'];
	     $bannersize             = $_FILES['banner']['size'];
	     $id = '1';
		
				
			

  
				
		
             $this->db->query("UPDATE `hometopbanner` set `bannerimage`=:IMG WHERE `id`=:ID");
				$update = $this->db->execute(array(
                   
					 ':IMG' => $banner,
					 ':ID' => $id,
                ));
			
				 if($update)
			  {
					 $pathfirst = '../image/';
					  // move_uploaded_file($photogalleryimageTemp,$photogallerypath.$photogalleryimageRandom);
					 move_uploaded_file( $bannerTemp,$pathfirst.$banner);
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'id' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            
	}


  public function updateBannerDetail($heading,$bannercontent)
  {
	  if (isset($heading) && isset($bannercontent) ) {
               $heading = addslashes(trim(filter_var($heading, FILTER_SANITIZE_STRING)));
                $bannercontent = addslashes(trim($bannercontent, FILTER_SANITIZE_STRING));
		       $id = '1';
			   
		 if (!Validation::validateTitle($heading)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid heading"
                ];
              
            }
		 else if (!Validation::validateFirstContent($bannercontent)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Content"
                ];
              
            }
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `Bannerheading` set `heading`=:TITLE,`description`=:FIRSTCONTENT WHERE `id`=:ID");
				$update = $this->db->execute(array(
                    ':TITLE' => $heading,
					':FIRSTCONTENT' => $bannercontent,
					':ID' => $id,
                ));
			
				 if($update)
			  {
					 
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'returnid' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content ! please try again"
                ];
                }
  }
	
	
	public function updateProgramDetail($heading,$bannercontent)
  {
	  if (isset($heading) && isset($bannercontent) ) {
               $heading = addslashes(trim(filter_var($heading, FILTER_SANITIZE_STRING)));
                $bannercontent = addslashes(trim($bannercontent, FILTER_SANITIZE_STRING));
		       $id = '1';
			   
		 if (!Validation::validateTitle($heading)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid heading"
                ];
              
            }
		 else if (!Validation::validateFirstContent($bannercontent)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Content"
                ];
              
            }
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `programe` set `programeheading`=:TITLE,`description`=:FIRSTCONTENT WHERE `id`=:ID");
				$update = $this->db->execute(array(
                    ':TITLE' => $heading,
					':FIRSTCONTENT' => $bannercontent,
					':ID' => $id,
                ));
			
				 if($update)
			  {
					 
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'returnid' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content ! please try again"
                ];
                }
  }


	public function updateFindYourSelfDetail($heading,$bannercontent)
  {
	  if (isset($heading) && isset($bannercontent) ) {
               $heading = addslashes(trim(filter_var($heading, FILTER_SANITIZE_STRING)));
                $bannercontent = addslashes(trim($bannercontent, FILTER_SANITIZE_STRING));
		       $id = '1';
			   
		 if (!Validation::validateTitle($heading)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid heading"
                ];
              
            }
		 else if (!Validation::validateFirstContent($bannercontent)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Content"
                ];
              
            }
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `findyourself` set `heading`=:TITLE,`description`=:FIRSTCONTENT WHERE `id`=:ID");
				$update = $this->db->execute(array(
                    ':TITLE' => $heading,
					':FIRSTCONTENT' => $bannercontent,
					':ID' => $id,
                ));
			
				 if($update)
			  {
					 
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'returnid' => $id,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid title or content ! please try again"
                ];
                }
  }


	public function getBannerDetail()
	  { 
     $this->db->query("select * from Bannerheading");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();
     return $row;
  }
	}
	
	public function getProgrameDetail()
	  { 
     $this->db->query("select * from programe");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();
     return $row;
  }
	}
	public function getNewProgrameDetail($progid)
	  { 
     $this->db->query("select * from programe where id	=:ID");
      $exe =  $this->db->execute(array(
        ":ID" => $progid
         
    ));
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();
     return $row;
  }
	}
	public function getFindYourSelfDetail()
	  { 
     $this->db->query("select * from findyourself");
     $exe =  $this->db->execute();
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetch();
     return $row;
  }
	}
}