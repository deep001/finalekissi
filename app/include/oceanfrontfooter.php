<?php
require_once __DIR__ . '/../../autoload/define.php';
session_start();
use App\Classes\Config;
?>
<footer class="">
<div class="bg-gray-4000">
<div class="container px-md-5">
<div class="d-flex flex-wrap align-items-center pt-6 pb-3d border-bottom mb-7 border-gray-4100">
<a href="index-2.html" class="mb-4 mb-md-0 mr-auto">
<!--<svg version="1.1" width="103" height="40px">
<g style="fill: #ffffff;">
<path class="vodi-stream0" d="M72.8,12.5c0-2.7,0-1.8,0-4.4c0-0.9,0-1.8,0-2.8C73,2.8,74.7,1.2,77,1.2s4.1,1.8,4.2,4.2c0,1,0,2.1,0,3.1
                                c0,6.5,0,9.4,0,15.9c0,4.7-1.7,8.8-5.6,11.5c-4.5,3.1-9.3,3.5-14.1,0.9c-4.7-2.5-7.1-6.7-7-12.1c0.1-7.8,6.3-13.6,14.1-13.2
                                c0.7,0,1.4,0.2,2.1,0.3C71.3,12,72,12.2,72.8,12.5z M67.8,19.6c-2.9,0-5.2,2.2-5.2,5c0,2.9,2.3,5.3,5.2,5.3c2.8,0,5.2-2.4,5.2-5.2
                                C73,22,70.6,19.6,67.8,19.6z"></path>
<path class="vodi-stream0" d="M39.9,38.4c-7.3,0-13.3-6.1-13.3-13.5c0-7.5,5.9-13.4,13.4-13.4s13.4,6,13.4,13.5
                                C53.4,32.4,47.4,38.4,39.9,38.4z M39.9,30.4c3.2,0,5.6-2.3,5.6-5.6c0-3.2-2.3-5.5-5.5-5.5s-5.6,2.2-5.6,5.4
                                C34.4,28,36.7,30.4,39.9,30.4z"></path>
<path class="vodi-stream0" d="M14.6,26.8c0.6-1.4,1.1-2.6,1.6-3.8c1.2-2.9,2.5-5.8,3.7-8.8c0.7-1.7,2-2.8,4-2.7c3,0,4.9,2.6,3.8,5.4
                                c-0.5,1.3-1.2,2.6-1.8,3.9c-2.4,5-4.9,10-7.3,15c-0.8,1.6-2,2.6-3.9,2.6c-2,0-3.3-0.8-4.2-2.6c-2.7-5.6-5.3-11.1-8-16.7
                                c-0.3-0.7-0.6-1.3-0.9-2c-0.8-1.8-0.3-3.7,1.1-4.8c1.5-1.2,4-1.3,5.3,0c0.7,0.6,1.2,1.5,1.6,2.3C11.3,18.6,12.9,22.5,14.6,26.8z"></path>
<path class="vodi-stream0" d="M90.9,24.9c0,3.1,0,6.2,0,9.4c0,1.9-1.2,3.4-2.9,4c-1.7,0.5-3.5,0-4.5-1.6c-0.5-0.8-0.8-1.8-0.8-2.6
                                c-0.1-6.1-0.1-11.3,0-17.5c0-2.2,1.5-3.9,3.5-4.2c2.1-0.3,4.1,0.9,4.7,2.9c0.1,0.5,0.2,1.1,0.2,1.6C90.9,19.8,90.9,21.9,90.9,24.9
                                L90.9,24.9z"></path>
<path style="fill: #51C9F0;" d="M90.2,4.5L86,2.1c-1.3-0.8-3,0.2-3,1.7v4.8c0,1.5,1.7,2.5,3,1.7l4.2-2.4C91.5,7.2,91.5,5.3,90.2,4.5z"></path>
</g>
</svg>-->
<img src="assets/img/logotv.png" style="width:150px !important;height:auto !important;" >
</a>
<ul class="list-unstyled mx-n3 mb-0 d-flex flex-wrap align-items-center">
<li class="px-3">
<a href="#" class="text-gray-1300 d-flex flex-wrap align-items-center"><!--<i class="fab fa-facebook-f fa-inverse"></i>--><span class="ml-2">Facebook</span></a>
</li>
<li class="px-3">
<a href="#" class="text-gray-1300 d-flex flex-wrap align-items-center"><!--<i class="fab fa-twitter fa-inverse"></i>--><span class="ml-2">Twitter</span></a>
</li>
<li class="px-3">
<a href="#" class="text-gray-1300 d-flex flex-wrap align-items-center"><!--<i class="fab fa-google-plus-g fa-inverse"></i>--><span class="ml-2">Google+</span></a>
</li>
<li class="px-3">
<a href="#" class="text-gray-1300 d-flex flex-wrap align-items-center"><!--<i class="fab fa-vimeo-v fa-inverse"></i>--><span class="ml-2">Vimeo</span></a>
</li>
<li class="px-3">
<a href="#" class="text-gray-1300 d-flex flex-wrap align-items-center"><!--<i class="fas fa-rss fa-inverse"></i>--><span class="ml-2">RSS</span></a>
</li>
</ul>
</div>
<div class="row pb-5">
<div class="col-md mb-5 mb-md-0">
<h4 class="font-size-18 font-weight-medium mb-4 text-gray-4200">Movie Categories</h4>
<ul class="column-count-2 v2 list-unstyled mb-0">
<li class="py-1d">
<a class="h-g-white" href="#">Action</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Adventure</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Animation</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Comedy</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Crime</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Drama</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Fantacy</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Horror</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Mystrey</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Romance</a>
</li>
</ul>
</div>
<div class="col-md mb-5 mb-md-0">
<h4 class="font-size-18 font-weight-medium mb-4 text-gray-4200">TV Series</h4>
<ul class="column-count-2 v2 list-unstyled mb-0">
<li class="py-1d">
<a class="h-g-white" href="#">Valentine Day</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Underrated Comedies</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Scary TV Series</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Best 2020 Documentaries</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Classic Shows</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Big TV Premieres</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Reality TV Shows</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Original Shows</a>
 </li>
<li class="py-1d">
<a class="h-g-white" href="#">Suprise of the Year Shows</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Top 10 TV Shows</a>
</li>
</ul>
</div>
<div class="col-md-2 mb-5 mb-md-0 border-left border-gray-4100">
<div class="ml-1">
<h4 class="font-size-18 font-weight-medium mb-4 text-gray-4200">Pages</h4>
<!--<ul class="list-unstyled mb-0">
<li class="py-1d">
<a class="h-g-white" href="#">My Account</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">FAQ</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Watch on TV</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Help Center</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Contact</a>
</li>
</ul>-->
<ul class="list-unstyled mb-0">
<li class="py-1d">
<a class="h-g-white" href="aboutus.php">About us</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="privacypolicy.php">Privacy Policy </a>
</li>
<li class="py-1d">
<a class="h-g-white" href="terms.php">Terms</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Help Center</a>
</li>
<li class="py-1d">
<a class="h-g-white" href="#">Contact</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="bg-gray-4300">
<div class="container px-md-5">
<div class="text-center d-md-flex flex-wrap align-items-center py-3">
<div class="font-size-13 text-gray-1300 mb-2 mb-md-0">Copyright © 2020, Ocean TV All Rights Reserved | Powered By Rankmantra</div>
<!--<a href="#" class="font-size-13 h-g-white ml-md-auto">Privacy Policy</a>-->
</div>
</div>
</div>
</footer>