<?php 

namespace App\Classes;
use App\Traits\Databasetraits;

session_start();

class Sqlthree
{
    
    use Databasetraits;
	public function runQuery($sqlquery)
	{
		if(isset($sqlquery))
		{
			 $sqlquery = addslashes(trim(filter_var($sqlquery, FILTER_SANITIZE_STRING)));
			 $this->db->query($sqlquery);
             $exe =  $this->db->execute();
             if ($this->db->rowCount() > 0) {
             $row = $this->db->fetchAll();
		     return $row;
     
     
		}
	}
	}
	
	public function firstQuery($sqlquery)
	{
		if(isset($sqlquery))
		{
			 $sqlquery = addslashes(trim(filter_var($sqlquery, FILTER_SANITIZE_STRING)));
			 $this->db->query($sqlquery);
             $exe =  $this->db->execute();
             if ($this->db->rowCount() > 0) {
             $row = $this->db->fetchAll();
		     return $row;
     
     
		}
	}
	}
	
	public function runTableQuery($tablename)
	{
		 $tablename = addslashes(trim(filter_var($tablename, FILTER_SANITIZE_STRING)));   
			 $this->db->query("select * from $tablename");
             $exe =  $this->db->execute();
             if ($this->db->rowCount() > 0) {
             $row = $this->db->fetchAll();
             return $row;      
	}
	}
   public function verifySignInRequest($status,$role,$userEmail,$userPassword)
   {
	    if (isset($userEmail) && isset($userPassword) && isset($status) && isset($role)) {
                $userEmail = trim(strtolower(filter_var($userEmail, FILTER_SANITIZE_EMAIL)));
                $userPassword = trim(filter_var($userPassword, FILTER_SANITIZE_STRING));
			    $role = trim(filter_var($role, FILTER_SANITIZE_STRING));
			  
			    
          //  $gRecaptchaResponse = trim(filter_var($gRecaptchaResponse, FILTER_SANITIZE_STRING));

       /*     $googleObj = new Googlelib();
            if (!$googleObj->gRecaptchaValidate($gRecaptchaResponse)) {
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid googleRecaptchaValidate recaptcha, Please try again"
                ];
                  
            } */
				 if (!Validation::validateEmail($userEmail)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid email"
                ];
              
            } else if (!Validation::validatePasswordStrength($userPassword)) {

                return (object)[
                    'status'=>false,
                    'msg'=>"invalid password strength, it can contain atleast 1 upper and 1 lower alphabet, 1 number, 1 special character @!#&%*"
                ];
              
            } 
			/*else if (!$this->verifyEmailnPassword($userEmail, $userPassword)) {
             
                return (object)[
                    'status'=>false,
                    'msg'=>"incorrect email or password"
                ];
              
            }*/
			else { 
				
			
                $finalp = base64_encode($userPassword);
				
				$this->db->query("UPDATE `login` set `STATUS`=:ST WHERE `EMAILID`=:MAIL");
				$update = $this->db->execute(array(
                    ':ST' => $status,
					 ':MAIL' => $userEmail,
					
                ));
			
				if($update) 
				{
              /*  $this->db->query("select LOGINID,EMAILID from login where EMAILID=:EMAIL and PASSWORD=:PASSWORD and USER_TYPE=:ROLE and STATUS=:STAT"); 
                 $this->db->execute(array(
                    ":EMAIL" => $userEmail,
					 ":PASSWORD" => $finalp,
					 ":ROLE" => $role,
					 ":STAT" => $status,
                   
                ));
                if ($this->db->rowCount() > 0) {
                    $row = $this->db->fetch();
                  return (object)[
                        'status'=>true,
                        'email'=>$row->EMAILID,
                        'id'=>$row->LOGINID,
					    'role' => $row->USER_TYPE,

                    ];  
			
					
                    
                }else{
                 
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid email id or password or user status are not verified yet ! please try again"
                ];
                } */
                 return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'userreturnid' => $userEmail,
                    
                ];
            }
				else{
                 
					 return (object)[
                    'status'=>false,
                    'msg'=>"User status still not Active ! Please try again and verify your account by click on verification link"
                ];
                }
			}
        } else {
           return Errorlist::errorResponse(false, "incorrect email or password");
        }
   }
}