<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\ChannelLogin;
use App\Classes\Csrf;
use App\Classes\Headers;

session_start();
if(isset($_POST["Signin"]))
{
	 $validationcsrf = new stdClass();
	//if(isset($_POST['role']) && isset($_POST["Email"]) && isset($_POST['password']) && isset($_POST['csrf-token']))
	if(isset($_POST["email"]) && isset($_POST['password']) && isset($_POST['csrf-token']))
	{
		      //    $status = 'Active';
		      //    $role = filter_input(INPUT_POST, 'role', FILTER_SANITIZE_STRING);
		          $userEmail = strtolower(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL));
                  $userPassword = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
		          
                 
              // $gRecaptchaResponse = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_SANITIZE_STRING);
                  $csrfToken = filter_input(INPUT_POST, 'csrf-token', FILTER_SANITIZE_STRING); 
		
		   if ($_SESSION['csrf'] != $csrfToken) {
          

            $validationcsrf->status = "FALSE";
            $validationcsrf->msg = "Invalid token, Please try again!!";
        
         
        }
		else
			{
            $login = new ChannelLogin();
            $signinresponse = $login->getSignInRequest($userEmail, $userPassword);
           if($signinresponse->status == true)
			{
			    $PIN= "OCEAN".rand();
			    $_SESSION['id']=$PIN; //Random user id
			    $INV= "INV".rand(); //Invoice Generate
	            $_SESSION['inv']=$INV; 
				$_SESSION['u_email'] = $signinresponse->email;
                $_SESSION['uid'] = $signinresponse->id; //userid
			    $_SESSION['userrole'] = $signinresponse->role;
			    $_SESSION['username'] = $signinresponse->username;
                unset($_SESSION['csrf']);
			//    if($_SESSION['userrole'] == 'User') {
                Headers::redirect("/project/ocnew/index.php"); 
			//	} else {
			//	Headers::redirect("/admin/schoollist.php");	
			//	}
            }
			
			else if ($signinresponse->status == false) {
                $responseerror =  $signinresponse->msg;	
                  
            }
			
        }
	}
}


$getcsrf = Csrf::getCsrfToken();
$_SESSION['csrf'] = $getcsrf;

?>

<!DOCTYPE html>
<html lang="en">
<head>

<title> Comming Soon | Ocean</title>
<?php include_once Config::path()->INCLUDE_PATH.'/oceanfronthead.php'; ?>
</head>
<body>
<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontheadernew.php'; ?>



<main id="content">
<div class="bg-gray-1000 py-6 pt-lg-9 pb-lg-11 mb-7 mb-lg-10">
<div class="container px-md-5 mb-lg-2">
<h6 class="font-size-30 font-weight-semi-bold text-center mb-1" style="color:#000000 !important;">Thank you....</h6>
<nav aria-label="breadcrumb">
<ol class="breadcrumb justify-content-center p-0">
<li class="breadcrumb-item"><a href="index.php">Home</a></li>
<li class="breadcrumb-item active" aria-current="page" href="signin.php">Thank you for making shopping with us....</li>
</ol>
</nav>
</div>
</div>

</div>
<!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.835253576489!2d144.95372995111143!3d-37.817327679652266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1581584770980!5m2!1sen!2sin" height="320" style="border:0; width: 100%;" allowfullscreen=""></iframe>
<div class="container px-md-5">
<div class="space-1 space-lg-2">
<div class="py-lg-3">
<h6 class="font-size-24 font-weight-normal text-center mb-0">We Are Always Thinking Global</h6>
</div>
</div>
</div>-->
</main>


<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontfooter.php'; ?>


<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<button type="button" class="close position-absolute top-0 right-0 z-index-2 mt-3 mr-3" data-dismiss="modal" aria-label="Close">
<svg aria-hidden="true" class="mb-0" width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</button>

<div class="modal-body">
<form class="js-validate">

<div id="login">

<div class="text-center mb-7">
<h3 class="mb-0">Sign In to Vodi</h3>
<p>Login to manage your account.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signinEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-3">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signinPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
</div>
</div>

<div class="d-flex justify-content-end mb-4">
<a class="js-animation-link small link-underline" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#forgotPassword",
                                        "groupName": "idForm"
                                    }'>Forgot Password?
</a>
</div>
<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign In</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
Sign In with Google
</span>
</a>
<div class="text-center">
<span class="small text-muted">Do not have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#signup",
                                        "groupName": "idForm"
                                    }'>Sign Up
</a>
</div>
</div>

<div id="signup" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Create your account</h3>
<p>Fill out the form to get started.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signupEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signupPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Confirm Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="confirmPassword" id="signupConfirmPassword" placeholder="Confirm Password" aria-label="Confirm Password" required data-msg="Password does not match the confirm password.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign Up</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
Sign Up with Google
</span>
</a>
<div class="text-center">
<span class="small text-muted">Already have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Sign In
</a>
</div>
</div>


<div id="forgotPassword" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Recover password</h3>
<p>Instructions will be sent to you.</p>
</div>


<div class="js-form-message">
<label class="sr-only" for="recoverEmail">Your email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="recoverEmail" placeholder="Your email" aria-label="Your email" required data-msg="Please enter a valid email address.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Recover Password</button>
</div>
<div class="text-center mb-4">
<span class="small text-muted">Remember your password?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Login
</a>
</div>
</div>

</form>
</div>

</div>
</div>
</div>


<aside id="sidebarContent" class="hs-unfold-content sidebar sidebar-left off-canvas-menu">
<div class="sidebar-scroller">
<div class="sidebar-container">
<div class="sidebar-footer-offset" style="padding-bottom: 7rem;">

<div class="d-flex justify-content-end align-items-center py-2 px-4 border-bottom">

<a class="navbar-brand mr-auto" href="../home/index.html" aria-label="Vodi">
<svg version="1.1" width="103" height="40px">
<linearGradient id="vodi-gr1" x1="0%" y1="0%" x2="100%" y2="0%">
<stop offset="0" style="stop-color:#2A4999"></stop>
<stop offset="1" style="stop-color:#2C9CD4"></stop>
</linearGradient>
<g class="vodi-gr">
<path class="vodi-svg0" d="M72.8,12.7c0-2.7,0-1.8,0-4.4c0-0.9,0-1.8,0-2.8C73,3,74.7,1.4,77,1.4c2.3,0,4.1,1.8,4.2,4.2c0,1,0,2.1,0,3.1
                                    c0,6.5,0,9.4,0,15.9c0,4.7-1.7,8.8-5.6,11.5c-4.5,3.1-9.3,3.5-14.1,0.9c-4.7-2.5-7.1-6.7-7-12.1c0.1-7.8,6.3-13.6,14.1-13.2
                                    c0.7,0,1.4,0.2,2.1,0.3C71.3,12.2,72,12.4,72.8,12.7z M67.8,19.8c-2.9,0-5.2,2.2-5.2,5c0,2.9,2.3,5.3,5.2,5.3
                                    c2.8,0,5.2-2.4,5.2-5.2C73,22.2,70.6,19.8,67.8,19.8z
                                    M39.9,38.6c-7.3,0-13.3-6.1-13.3-13.5c0-7.5,5.9-13.4,13.4-13.4c7.5,0,13.4,6,13.4,13.5
                                    C53.4,32.6,47.4,38.6,39.9,38.6z M39.9,30.6c3.2,0,5.6-2.3,5.6-5.6c0-3.2-2.3-5.5-5.5-5.5c-3.2,0-5.6,2.2-5.6,5.4
                                    C34.4,28.2,36.7,30.6,39.9,30.6z
                                    M14.6,27c0.6-1.4,1.1-2.6,1.6-3.8c1.2-2.9,2.5-5.8,3.7-8.8c0.7-1.7,2-2.8,4-2.7c3,0,4.9,2.6,3.8,5.4
                                    c-0.5,1.3-1.2,2.6-1.8,3.9c-2.4,5-4.9,10-7.3,15c-0.8,1.6-2,2.6-3.9,2.6c-2,0-3.3-0.8-4.2-2.6c-2.7-5.6-5.3-11.1-8-16.7
                                    c-0.3-0.7-0.6-1.3-0.9-2c-0.8-1.8-0.3-3.7,1.1-4.8c1.5-1.2,4-1.3,5.3,0c0.7,0.6,1.2,1.5,1.6,2.3C11.3,18.8,12.9,22.7,14.6,27z
                                    M90.9,25.1c0,3.1,0,6.2,0,9.4c0,1.9-1.2,3.4-2.9,4c-1.7,0.5-3.5,0-4.5-1.6c-0.5-0.8-0.8-1.8-0.8-2.6
                                    c-0.1-6.1-0.1-11.3,0-17.5c0-2.2,1.5-3.9,3.5-4.2c2.1-0.3,4.1,0.9,4.7,2.9c0.1,0.5,0.2,1.1,0.2,1.6C90.9,20,90.9,22.1,90.9,25.1
                                    C90.9,25.1,90.9,25.1,90.9,25.1z
                                    M90.2,4.7L86,2.3c-1.3-0.8-3,0.2-3,1.7v4.8c0,1.5,1.7,2.5,3,1.7l4.2-2.4C91.5,7.4,91.5,5.5,90.2,4.7z"></path>
</g>
</svg>
</a>

<div class="hs-unfold">
<a class="js-hs-unfold-invoker btn btn-icon btn-xs btn-soft-secondary" href="javascript:;" data-hs-unfold-options='{
                                    "target": "#sidebarContent",
                                    "type": "css-animation",
                                    "animationIn": "fadeInLeft",
                                    "animationOut": "fadeOutLeft",
                                    "hasOverlay": "rgba(55, 125, 255, 0.1)",
                                    "smartPositionOff": true
                                }'>
<svg width="10" height="10" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</a>
</div>
</div>


<div class="scrollbar sidebar-body">
<div class="sidebar-content py-4">

<div class="">
<div id="sidebarNavExample3" class="collapse show navbar-collapse">

<div class="sidebar-body_inner">
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav1Collapse" data-target="#sidebarNav1Collapse">
Home
</a>
<div id="sidebarNav1Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav1" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../home/index.html">Home v1</a>
<a class="dropdown-item" href="../home/home-v2.html">Home v2</a>
<a class="dropdown-item" href="../home/home-v3.html">Home v3</a>
<a class="dropdown-item" href="../home/home-v4.html">Home v4</a>
<a class="dropdown-item" href="../home/home-v5.html">Home v5</a>
<a class="dropdown-item" href="../home/home-v6.html">Home v6 - Vodi Prime (Light)</a>
<a class="dropdown-item" href="../home/home-v7.html">Home v7 - Vodi Prime (Dark)</a>
<a class="dropdown-item" href="../home/home-v8.html">Home v8 - Vodi Stream</a>
<a class="dropdown-item" href="../home/home-v9.html">Home v9 - Vodi Tube (Light)</a>
<a class="dropdown-item" href="../home/home-v10.html">Home v10 - Vodi Tube (Dark)</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Collapse" data-target="#sidebarNav2Collapse">
Archive Pages
</a>
<div id="sidebarNav2Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav2" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../archive/movies.html">Movies</a>
<a class="dropdown-item" href="../archive/tv-shows.html">TV Shows</a>
<a class="dropdown-item" href="../archive/videos.html">Videos</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2One">
Single Movies
</a>
<div id="sidebarNav2One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-movies/single-movies-v1.html">Movie v1</a>
<a class="dropdown-item" href="../single-movies/single-movies-v2.html">Movie v2</a>
<a class="dropdown-item" href="../single-movies/single-movies-v3.html">Movie v3</a>
<a class="dropdown-item" href="../single-movies/single-movies-v4.html">Movie v4</a>
<a class="dropdown-item" href="../single-movies/single-movies-v5.html">Movie v5</a>
<a class="dropdown-item" href="../single-movies/single-movies-v6.html">Movie v6</a>
<a class="dropdown-item" href="../single-movies/single-movies-v7.html">Movie v7</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Two">
Single Videos
</a>
<div id="sidebarNav2Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-video/single-video-v1.html">Video v1</a>
<a class="dropdown-item" href="../single-video/single-video-v2.html">Video v2</a>
<a class="dropdown-item" href="../single-video/single-video-v3.html">Video v3</a>
<a class="dropdown-item" href="../single-video/single-video-v4.html">Video v4</a>
<a class="dropdown-item" href="../single-video/single-video-v5.html">Video v5</a>
<a class="dropdown-item" href="../single-video/single-video-v6.html">Video v6</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Collapse" data-target="#sidebarNav3Collapse">
Single Episodes
</a>
<div id="sidebarNav3Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav3" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../single-episodes/single-episodes-v1.html">Episode v1</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v2.html">Episode v2</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v3.html">Episode v3</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v4.html">Episode v4</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3One">
Other Pages
</a>
<div id="sidebarNav3One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../other/landing-v1.html">Landing v1</a>
<a class="dropdown-item" href="../other/landing-v2.html">Landing v2</a>
<a class="dropdown-item" href="../other/coming-soon.html">Coming Soon</a>
<a class="dropdown-item" href="../single-tv-show/single-tv-show.html">Single TV Show</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Two">
Blog Pages
</a>
<div id="sidebarNav3Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../blog/blog.html">Blog</a>
<a class="dropdown-item" href="../blog/blog-single.html">Single Blog</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4Collapse" data-target="#sidebarNav4Collapse">
Static Pages
</a>
<div id="sidebarNav4Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav4" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../static/contact.html">Contact Us</a>
<a class="dropdown-item" href="../static/404.html">404</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav4One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4One">
Docs
</a>
<div id="sidebarNav4One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav4">
<a class="dropdown-item" href="../../documentation/index.html">Documentation</a>
<a class="dropdown-item" href="../../snippets/index.html">Snippets</a>
</div>
</div>

</div>
</div>
</div>

</div>
</div>

</div>
</div>
</div>
</aside>



<a class="js-go-to go-to position-fixed" href="javascript:;" style="visibility: hidden;" data-hs-go-to-options='{
            "offsetTop": 700,
            "position": {
                "init": {
                    "right": 15
                },
                "show": {
                    "bottom": 15
                },
                "hide": {
                    "bottom": -15
                }
            }
        }'>
<i class="fas fa-angle-up"></i>
</a>


<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontscript.php'; ?>
</body>
</html>


