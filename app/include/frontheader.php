<?php
require_once __DIR__ . '/../../autoload/define.php';
use App\Classes\Config;
?>



<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">
									<li><div class="question">Have any questions?</div></li>
									<li>
										<div>(009) 35475 6688933 32</div>
									</li>
									<li>
										<div>info@elaerntemplate.com</div>
									</li>
								</ul>
								<div class="top_bar_login ml-auto">
								  
						
									<ul>
									    <?php if(!empty($_SESSION['u_email'])){ ?>
										<li><a href="#">Welcome &nbsp;<?php echo $_SESSION['u_email'];?>
									</a></li>
										<li><a href="logout.php">Logout</a></li>
										<li><a href="changepassword.php">Change Password</a></li>
										<?php } else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) {  ?>
										<li><img src="https://graph.facebook.com/<?php echo $_SESSION['FBID']; ?>/picture"></li>
									    <li><a href="#">Welcome &nbsp;<?php echo $_SESSION['FULLNAME'];?></a></li>
										<li><a href="logof.php">Logout</a></li>
										<?php } 
									else if(!empty($_SESSION['user_email_address']) && !empty($_SESSION['user_first_name'])) {  ?>
										<li><img src="<?php echo $_SESSION['user_image']; ?>" style="width:74px;height:50px;"></li>
									    <li><a href="#">Welcome &nbsp;<?php echo $_SESSION['user_first_name'];?></a></li>
										<li><a href="/google-login/logout.php">Logout</a></li>
									<?php  } 
										else { ?>
										 <li><a href="register.php">Register</a></li>
										<li><a href="login.php">Login</a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="#">
									<div class="logo_content d-flex flex-row align-items-end justify-content-start">
										<div class="logo_img"><img src="abb/img/logo.jpg" alt=""></div>
										
									</div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
								<ul class="main_nav">
									<li class="sqldropdown"><div class="sqldropbtn"><a href="#">sql</a></div>
									<div class="sqldropdown-content">
    <a href="index.php">SQL Test 1</a>
	<a href="sqltest2.php">SQL Test 2</a>
	<a href="sqltest3.php">SQL Test 3</a>
    <a href="sqltest4.php">SQL Test 4</a>
	<a href="sqltest5.php">SQL Test 5</a>
										
   
  </div></li>
									<li class="active"><a href="about.php">about us</a></li>
									
									<li><a href="resources.php">Resources</a></li>
									<li><a href="contact.php">contact</a></li>
									<?php if(!empty($_SESSION['u_email'])){ ?>
									<li><a href="logout.php">Logout</a></li>
									<?php } else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) { ?>
									
									<li><a href="logof.php">Logout</a></li>
									<?php } 
									else if(!empty($_SESSION['user_email_address']) && !empty($_SESSION['user_first_name'])) {  ?>
										
										<li><a href="/google-login/logout.php">Logout</a></li>
									<?php  } 
									else { ?>
									    <li><a href="register.php">Register</a></li>
										<li><a href="login.php">Login</a></li>
										<?php } ?>
									
								</ul>
								<div class="search_button"><i class="fa fa-search" aria-hidden="true"></i></div>

								<!-- Hamburger -->

								<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true"></i>
								</div>
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Search Panel -->
		<div class="header_search_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_search_content d-flex flex-row align-items-center justify-content-end">
							<form action="#" class="header_search_form">
								<input type="search" class="search_input" placeholder="Search" required="required">
								<button class="header_search_button d-flex flex-column align-items-center justify-content-center">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>			
		</div>			
	</header>

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="search">
			<form action="#" class="header_search_form menu_mm">
				<input type="search" class="search_input menu_mm" placeholder="Search" required="required">
				<button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="menu_nav">
			<ul class="menu_mm">
				
				<li class="menu_mm"><a href="index.php">SQL Test 1</a></li>
					<li class="menu_mm"><a href="sqltest2.php">SQL Test 2</a></li>
			
    
	
	<li class="menu_mm"><a href="sqltest3.php">SQL Test 3</a></li>
    <li class="menu_mm"><a href="sqltest4.php">SQL Test 4</a></li>
	<li class="menu_mm"><a href="sqltest5.php">SQL Test 5</a></li>
   
				<li class="menu_mm"><a href="about.php">About us</a></li>
				<li class="menu_mm"><a href="resources.php">Resources</a></li>
				
				<li class="menu_mm"><a href="contact.php">Contact Us</a></li>
				<?php if(!empty($_SESSION['u_email'])){ ?>
				<li class="menu_mm"><a href="logout.php">Logout</a></li>
					<?php }  else if(!empty($_SESSION['FBID']) && !empty($_SESSION['EMAIL'])) { ?>
									
									<li><a href="logof.php">Logout</a></li>
									<?php } 
				else if(!empty($_SESSION['user_email_address']) && !empty($_SESSION['user_first_name'])) {  ?>
										
										<li><a href="/google-login/logout.php">Logout</a></li>
									<?php  } 
				else { ?>
				<li class="menu_mm"><a href="login.php">Login</a></li>	
				<?php } ?>
			</ul>
		</nav>
		<div class="menu_extra">
			<div class="menu_phone"><span class="menu_title">phone:</span>(009) 35475 6688933 32</div>
			<div class="menu_social">
				<span class="menu_title">follow us</span>
				<ul>
					<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>



	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158168493-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158168493-1');
</script>





