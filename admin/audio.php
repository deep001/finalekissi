<?php
session_start();
require_once __DIR__ . '/../autoload/define.php';
use App\Classes\Config;
//use App\Classes\Homepage;
use App\Classes\Audio;
use App\Classes\Headers;

//$logo = new Homepage();
//$getlogo = $logo->getLogoList();

$audio = new Audio();
$getaudiolist = $audio->getAudioRecordCompleteDetail();

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>List of all video - Music</title>
	<?php include_once Config::path()->INCLUDE_PATH.'/oceanadminhead.php'; ?>
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	<?php include_once Config::path()->INCLUDE_PATH.'/oceannewadminheader.php'; ?>
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			<?php include_once Config::path()->INCLUDE_PATH.'/oceannewadminleftsidebar.php'; ?>
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="/admin/schoollist.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="/admin/audio.php">Video</a> </li>
						
						<li class="page-back"><a href="/admin/audio.php"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>Logo</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="/admin/schoollist" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
						<ul id="dr-list" class="dropdown-content">
							<li><a href="/admin/schoollist">Add New</a> </li>
							<li><a href="/admin/schoollist">Edit</a> </li>
							<li><a href="/admin/schoollist">Update</a> </li>
							<li class="divider"></li>
							<li><a href="/admin/schoollist"><i class="material-icons">delete</i>Delete</a> </li>
							<li><a href="/admin/schoollist"><i class="material-icons">subject</i>View All</a> </li>
							<li><a href="/admin/schoollist"><i class="material-icons">play_for_work</i>Download</a> </li>
						</ul>
						<!-- Dropdown Structure -->
						<div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp ad-inn-page">
									<div class="tab-inn ad-tab-inn">
										<div class="table-responsive">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>Select</th>
														
														<th>Name</th>
														<th>Image</th>
														<th>Audio Description</th>
														<th>Iframe</th>
														<th>Edit</th>
														<th>Del</th>
														
													</tr>
												</thead>
												<tbody>
													<?php	 $sn = 1;
													foreach($getaudiolist as $roww)
		                    { ?>
													<tr>
														<td>
															<input type="checkbox" class="filled-in" id="filled-in-box-1" />
															<label for="filled-in-box-1"></label>
														</td>
														
														<td><?php echo $roww->audioname; ?></td>
														<td><img src="../img/bg-img/<?php echo $roww->audioimage;?>" style="height: 100px;" ></td>
														<td><?php echo $roww->audiodescription; ?></td>
														<td><iframe src="<?php echo stripslashes($roww->audioiframe);?>" frameborder="0" allowfullscreen></iframe></td>
														<td><a href = "/admin/updateaudio.php?audioid=<?php echo base64_encode($roww->audioid);?>" >EDIT</a></td>
														<td><a href = "" >DEL</a></td>
														
													</tr>
													<?php } ?>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="admin-pag-na">
									<ul class="pagination list-pagenat">
										<li class="disabled"><a href="admin-all-users.html#!!"><i class="material-icons">chevron_left</i></a> </li>
										<li class="active"><a href="admin-all-users.html#!">1</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!">2</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!">3</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!">4</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!">5</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!">6</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!">7</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!">8</a> </li>
										<li class="waves-effect"><a href="admin-all-users.html#!"><i class="material-icons">chevron_right</i></a> </li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
<?php include_once Config::path()->INCLUDE_PATH.'/oceanadminscript.php'; ?>
</body>

</html>