<?php
session_start();
require_once __DIR__ . '/../autoload/define.php';
use App\Classes\Config;
use App\Classes\PaySuccess;
use App\Classes\Audio;
use App\Classes\Headers;


        $tota=0;
		$quantity=0;
        $ship=0;



if(isset($_GET['paymentid']) && !empty($_GET['paymentid']) && isset($_GET['userid']) && !empty($_GET['userid']) && isset($_GET['randomid']) && !empty($_GET['randomid']))
{
	$paymentid = base64_decode($_GET['paymentid']);
	$userid = base64_decode($_GET['userid']);
	$randomid = base64_decode($_GET['randomid']);
	
	// user detail
	$getdetail =  new PaySuccess();
	$getuserdetail = $getdetail->getUserFullDetailForShip($userid,$randomid);
	
	// purchasing detail with invoice
	$getpurchasedetail = $getdetail->getPurchaseFullDetailInvoice($paymentid,$userid,$randomid);
	
}





?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>Invoice - Detail</title>
	<?php include_once Config::path()->INCLUDE_PATH.'/oceanadminhead.php'; ?>
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	
	<section>
		<div class="tz tz-invo-full">
			<!--CENTER SECTION-->
			<div class="tz-2 tz-invo-full1">
				<div class="tz-2-com tz-2-main">
					<div class="db-list-com tz-db-table">
						<div class="invoice">
							<div class="invoice-1">
								<div class="invoice-1-logo">
									<img src="../assets/img/logotv.png" alt="" style="width:150px !important;height:auto !important;"><span>invoice</span>
								</div>
								<div class="invoice-1-add">
									<div class="invoice-1-add-left">
										<h3><?php echo $getuserdetail->USERNAME;?></h3>
										<p><?php echo $getuserdetail->ADDRESS;?></p>
										<h5>Bill To</h5>
										<p>Email: <?php echo $getuserdetail->EMAILID;?></p>
									</div>
									<div class="invoice-1-add-right">
										<ul>
											<li><span>Invoice Number</span>ad4582456987</li>
											<li><span>Date</span> 07 Jul 2017</li>
											<li><span>Payment Terms</span> Due on receipt</li>
											<li><span>Due Date</span> 07 Jul 2017</li>
										</ul>
									</div>
								</div>
								<div class="invoice-1-tab">
									<table class="responsive-table bordered">
										<thead>
											<tr>
												<!--<th>Description</th>
												<th>Price</th>
												<th>Duration</th>
												<th>Subtotal</th>-->
												  <th>Product</th>
			                                      <th>Image</th>
			                                      <th>Price</th>
			                                      <th>Quantity</th>
			                                      <th>Total</th>
				                                  <th>Shipping</th>
											</tr>
										</thead>
										<tbody>
										<!--	<tr>
												<td>Premium listing update</td>
												<td>$50</td>
												<td>2 year</td>
												<td class="invo-sub">$100.00</td>									
											</tr>
											<tr>
												<td>Leads</td>
												<td>$100</td>
												<td>4 year</td>
												<td class="invo-sub">$400.00</td>									
											</tr>-->
											<?php $sn=1;  foreach($getpurchasedetail as $roww) 
		   { ?>	
												<tr>
												<td><?php echo $roww->product_name;?></td>
												<td><img src="../assets/img/122x183/<?php echo $roww->product_image;?>" style="width:100px;height:100px;"></td>
												<td class="invo-sub">$<?php echo $roww->price;?></td>
												<td><?php echo $roww->quantity1;?></td>	
											    <td class="invo-sub">$<?php echo $roww->crt_cost;?></td>
				                                <td class="invo-sub">$<?php echo $roww->shipping;?></td>
											   </tr>
											
											
												<?php  $tota +=$roww->crt_cost;
				                                       $quantity +=$roww->quantity1;
			                                           $ship+=$roww->shipping;
			  
											
											 } ?>
										</tbody>
									</table>								
								</div>
							</div>
							<div class="invoice-2">
								<div class="invoice-price">
									<table class="responsive-table bordered">
										<thead>
											<tr>
												<!--<th>Bank info</th>
												<th>Due By</th>
												<th>Total Due</th>-->
												
															  <th scope="col">Cart Subtotal</th>
			                                                  <th scope="col">Quantity</th>
			                                                  <th scope="col">Shipping</th>
			                                                  <th scope="col">Order Total</th>
											</tr>
										</thead>
										<tbody>
											<tr>
											<!--	<td>Premium listing update</td>
												<td id="invo-date">18 May '17</td>
												<td id="invo-tot">$500.00</td>			-->		
												
											    <td class="invo-sub">$<?php echo $tota; ?></td>
												<td id="invo-date"><?php echo $quantity; ?> </td>
												<td id="invo-tot">$<?php echo $ship;?> </td>
												<?php	$finaltotal = $tota +  $ship;?>
												<td id="invo-tot">$<?php echo $finaltotal;?> </td>
											</tr>											
										</tbody>
									</table>								
								</div>							
							</div>
							<div class="invoice-print">
							<p>Thank you,<br>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> <a href="db-payment.html" class="waves-effect waves-light btn-large">Pay Invoice</a><a href="db-invoice-download.html#!" class="waves-effect waves-light btn-large">Print</a> <a href="db-invoice-download.html#!" class="waves-effect waves-light btn-large">Download PDF</a> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--END DASHBOARD-->
	<?php include_once Config::path()->INCLUDE_PATH.'/oceanadminscript.php'; ?>
</body>

</html>