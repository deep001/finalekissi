<?php 

namespace App\Classes;
use App\Traits\Databasetraits;

session_start();

class PaySuccess
{
	
	
	// protected $from = "deepaknautiyal52@gmail.com";
     
	// protected $sub = "Congratulations! You have been successfully registered. Please verify your account";
	 
	
     use Databasetraits;
	
	public function getDetailForOrder($userid,$randomid)
	{

		 $this->db->query("SELECT user_id,pd_id,uid,invoiceno,GROUP_CONCAT(product_name) as pname,GROUP_CONCAT(product_image) as imagename FROM tbl_cart WHERE user_id=:USERID AND uid=:ID GROUP BY uid");  
		
		$exe =  $this->db->execute(array(
                    ':ID' => $randomid,
					':USERID' => $userid,
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
	
	 	 public function insertPaymentStatus($item_name,$item_number,$txn_id,$payment_gross,$totalshippingamount,$item_image,$currency_code,$payment_status,$userid,$randomid,$uname,$uemail,$invoice)
	{
			     $item_name = trim(filter_var($item_name, FILTER_SANITIZE_STRING));
				 $item_number = trim(filter_var($item_number, FILTER_SANITIZE_STRING));
				 $txn_id = trim(filter_var($txn_id, FILTER_SANITIZE_STRING));
		         $payment_gross = trim(filter_var($payment_gross, FILTER_SANITIZE_STRING));
			     $totalshippingamount = trim(filter_var($totalshippingamount, FILTER_SANITIZE_STRING));
			     $item_image = trim(filter_var($item_image, FILTER_SANITIZE_STRING));
				 $currency_code = trim(filter_var($currency_code, FILTER_SANITIZE_STRING));
			     $payment_status = trim(filter_var($payment_status, FILTER_SANITIZE_STRING));
				 $userid = $userid;
			     $randomid = $randomid;
		         $uname = trim(filter_var($uname, FILTER_SANITIZE_STRING));
				 $uemail = trim(filter_var($uemail, FILTER_VALIDATE_EMAIL));
			     $invoice = $invoice;
			
				
				
		
				
				
				
				
			    $this->db->query("INSERT INTO `payments`(`item_name`,`item_number`,`txn_id`,`payment_gross`,`totalshipping`,`productimages`,`currency_code`,`payment_status`,`userid`,`randomid`,`username`,`useremail`,`invoice`) VALUES (:ITMNM,:ITMNUM,:TXNID,:PAYGROSS,:SHIPP,:PRIMAGES,:CURRCODE,:PAYSTAT,:UID,:RID,:UNAME,:UEMAIL,:INVOICE)");
			    
			
					
				$insert = $this->db->execute(array(
					":ITMNM" => $item_name,
                    ":ITMNUM" => $item_number,
					":TXNID" => $txn_id,
					":PAYGROSS" => $payment_gross,
					":SHIPP" => $totalshippingamount,
					":PRIMAGES" => $item_image,
					":CURRCODE" => $currency_code,
					":PAYSTAT" => $payment_status,
					":UID" => $userid,
					":RID" => $randomid,
					":UNAME" => $uname,
					":UEMAIL" => $uemail,
					":INVOICE" => $invoice,
					
                ));
			
				 if($insert)
			  {  

					
				    return (object)[
                    'status'=>true,
                    'msg'=>"Payment Successfully Done.",	
                    
                    
                ];
			  
				 }
					 
					 
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Payment Process ! Please try again"
                ];
                }
				
				
					
		
	}
	
	public function checkPaymentStatus($username,$email,$uid)
	{
		         $username = trim(filter_var($username, FILTER_SANITIZE_STRING));
				 $email = trim(filter_var($email, FILTER_VALIDATE_EMAIL)); 
				 $this->db->query("select payment_status from payments where username=:USERNM and useremail=:USERMAIL and userid=:UID"); 
		         $this->db->execute(array(
                    ":USERNM" => $username,
					 ":USERMAIL" => $email,
					 ":UID" => $uid,
					 ));
		          if ($this->db->rowCount() > 0) {
                    $row = $this->db->fetch();
                  return (object)[
                        'status'=>true,
                        'paymentstatus'=>$row->payment_status,
                        

                    ];  
				//	return $row;
					
                    
                }
		        else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"No Payment yet!"
                ];
                }
		      


		
	}
	
	
	public function PaymentStatusList()
	{
		    //     $username = trim(filter_var($username, FILTER_SANITIZE_STRING));
			//	 $email = trim(filter_var($email, FILTER_VALIDATE_EMAIL));
				 $this->db->query("select * from payments order by payment_id DESC"); 
		         $this->db->execute();
		          if ($this->db->rowCount() > 0) {
                  $row = $this->db->fetchAll();
                  return $row;
				  }


		
	}
	
	
		public function getOrderRecord()
	{
		$this->db->query("select * from  payments ORDER BY payment_id DESC");
        $exe =  $this->db->execute();
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	
	public function getUserFullDetailForShip($userid,$randomid)
	{

		 $this->db->query("SELECT * from loginone WHERE LOGINID=:USERID");  
		
		$exe =  $this->db->execute(array(
                    ':USERID' => $userid,
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
	
	public function getPurchaseFullDetailInvoice($paymentid,$userid,$randomid)
	{
		 $this->db->query("SELECT payments . * , tbl_cart . *
FROM payments
JOIN tbl_cart ON payments.randomid = tbl_cart.uid
AND payments.userid = :USERID
AND payments.randomid = :ID");  
		$exe =  $this->db->execute(array(
                    ":ID" => $randomid,
					":USERID" => $userid,
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	
	
	
}
  
  