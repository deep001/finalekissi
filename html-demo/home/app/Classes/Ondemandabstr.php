<?php 

namespace App\Classes;


abstract class Ondemandabstr
{
    abstract protected function getFirstOnDemand();
	abstract protected function getSecondOnDemand();
	abstract protected function getThirdOnDemand();
	abstract protected function getFourthOnDemand();
	abstract protected function getFifthOnDemand();
	abstract protected function getSixthOnDemand();
	abstract protected function getFirstOnDemandCompleteDetail();
	
}