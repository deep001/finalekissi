<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;
use App\Classes\ChannelLogin;
use App\Classes\Audio;
use App\Classes\Cart;
use App\Classes\Csrf;
use App\Classes\Headers;
use App\Classes\Country;

?>
<!DOCTYPE html>
<html lang="en">
<head>

<title>Single Movies  | Ocean TV</title>

<?php include_once Config::path()->INCLUDE_PATH.'/oceanfronthead.php'; ?>
	
</head>
<body>
<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontheadernew.php'; ?>

<main id="content">
<div class="bg-gray-1100 pb-5 pb-md-9">
<div class="container px-md-5 mb-1">
<nav aria-label="breadcrumb">
<ol class="breadcrumb dark font-size-1">
<li class="breadcrumb-item"><a href="../home/index.html" class="text-gray-1300">Home</a></li>
<!--<li class="breadcrumb-item"><a href="../single-movies/single-movies-v3.html" class="text-gray-1300">Adventures</a></li>-->
<li class="breadcrumb-item text-white active" aria-current="page">Video</li>
</ol>
</nav>
<div id="fancyboxGallery">
<div class="row">
<div class="col-lg-12">
<div class="position-relative min-h-270rem mb-2d mr-xl-3">
<iframe class="position-absolute w-100 h-lg-down-100 position-xl-relative top-0 bottom-0 border-0" height="620" src="https://www.youtube.com/embed/Bl8INGEnrbQ" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
</div>
<!--<div class="col-lg">
<div class="pl-md-5">
<div class="row mx-n2d mb-5 mb-lg-7 pb-lg-1">
<div class="col-2 px-2d mt-5 mt-md-3">
<img class="img-fluid" src="assets/img/300x450/img6.jpg" alt="Image-Description">
</div>
<div class="col px-2d">
<div>
<div>
<h6 class="font-size-24 text-white font-weight-semi-bold font-secondary mb-1">Renegades</h6>
<ul class="list-unstyled nav nav-meta font-secondary mb-2">
<li class="text-white">2020</li>
<li class="text-white">1hr 46mins</li>
<li class="text-white">PG-</li>
</ul>
<div class="d-flex align-items-center text-gray-1300">
 <div>
<i class="far fa-eye font-size-18"></i>
<span class="font-size-12 font-weight-semi-bold ml-1">8.3k views</span>
</div>
<div class="ml-6">
<a href="#" class="text-gray-1300">
<i class="far fa-thumbs-up font-size-18"></i>
</a>
<span class="font-size-12 font-weight-semi-bold ml-1">38+</span>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="d-flex align-items-center">
<div class="d-flex">
<div>
<i class="fas fa-star text-primary font-size-42"></i>
</div>
<div class="text-lh-1 ml-1">
<div class="text-primary font-size-24 font-weight-semi-bold">8.0</div>
<span class="text-gray-1300 font-size-12">1 Vote</span>
</div>
</div>
<div class="d-flex align-items-center ml-5 text-gray-1300">
<div>
<i class="far fa-heart font-size-30"></i>
</div>
<a href="#" class="text-gray-1300 ml-2">+ Playlist</a>
</div>
</div>
</div>
</div>-->
</div>
</div>
</div>
</div>
<!--<div class="bg-gray-3100 pt-5 pb-6">
<div class="container px-md-5">
<h6 class="text-white font-size-22 font-secondary font-weight-medium mb-4">You Also May Like</h6>
<div class="row row-cols-1 row-cols-md-3 row-cols-lg-4 row-cols-xl-8 mx-n2 dark">
<div class="col-xl px-2">
<div class="product mb-5 mb-xl-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/300x450/img12.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Fantacys</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">Journey's End</a></div>
</div>
</div>
<div class="col-xl px-2">
<div class="product mb-5 mb-xl-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/300x450/img14.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Horror</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">The Strangers: Prey At Night</a></div>
</div>
</div>
<div class="col-xl px-2">
<div class="product mb-5 mb-xl-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/300x450/img16.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Adventures</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">The Maze Runner</a></div>
</div>
</div>
<div class="col-xl px-2">
<div class="product mb-5 mb-xl-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid" src="assets/img/300x450/img18.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Mystery</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">Don Of Thieves</a></div>
</div>
</div>
<div class="col-xl px-2">
<div class="product mb-5 mb-lg-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/300x450/img4.jpg" alt="Image Description">
</a>
</div>
 <div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Family</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">I Can Only Imagine</a></div>
</div>
</div>
<div class="col-xl px-2">
<div class="product mb-5 mb-lg-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/300x450/img20.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Comedy</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">The Begin Again</a></div>
</div>
</div>
<div class="col-xl px-2">
<div class="product mb-5 mb-md-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<span class="position-absolute px-2d line-height-lg bg-primary rounded content-centered-x z-index-2 mt-n2 text-white font-size-12">Featured</span>
<img class="img-fluid" src="assets/img/300x450/img22.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Comedy</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">Midnight Sun</a></div>
</div>
</div>
<div class="col-xl px-2">
<div class="product mb-0">
<div class="product-image mb-2">
<a href="../single-movies/single-movies-v3.html" class="d-inline-block position-relative stretched-link">
<img class="img-fluid" src="assets/img/300x450/img24.jpg" alt="Image Description">
</a>
</div>
<div class="product-meta font-size-12 mb-1">
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">2020</a></span>
<span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Action</a></span>
 <span><a href="../single-movies/single-movies-v3.html" class="h-g-primary">Adventures</a></span>
</div>
<div class="product-title font-weight-bold font-size-1"><a href="../single-movies/single-movies-v3.html">Renegades</a></div>
</div>
</div>
</div>
</div>
</div>-->
<!--<div class="bg-gray-1100 py-5 pb-lg-10">
<div class="container px-md-5 mb-2">
<div>
<div class="font-size-24 font-weight-medium font-secondary text-white mb-4">We Recommend</div>
<div class="section-hot-premier-show">
<div class="row mx-n2">
<div class="col-md-6 col-lg-3 px-2">
<div class="position-relative dark mb-4 mb-lg-0">
<div class="movie_poster">
<div>
<img class="img-fluid" src="assets/img/600x900/img5.jpg" alt="Image-Description">
</div>
</div>
<div class="position-absolute bottom-0">
<div class="pl-3 pr-5 pb-3">
<h6 class="text-center product-title font-size-3 mb-2">
<a href="../single-movies/single-movies-v3.html">I Can Only Imagine</a>
</h6>
<p class="text-white text-center font-size-1 max-h-42rem overflow-hidden">The inspiring and unknown true story behind MercyMe's beloved, chart topping song that brings ultimate hope to so many is a gripping reminder of the power of true forgiveness.</p>
<div class="pb-1">
<a href="../single-movies/single-movies-v3.html" class="btn btn-outline-white py-3 w-100 btn-sm" tabindex="0">WATCH NOW</a>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 px-2">
<div class="position-relative dark mb-4 mb-lg-0">
<div class="movie_poster">
<div>
<img class="img-fluid" src="assets/img/600x900/img6.jpg" alt="Image-Description">
</div>
</div>
<div class="position-absolute bottom-0 pb-3">
<div class="pl-5 pr-6">
<h6 class="text-center product-title font-size-3 mb-2">
<a href="../single-movies/single-movies-v3.html">Breath</a>
</h6>
<p class="text-white text-center font-size-1 max-h-42rem overflow-hidden">he inspiring and unknown true story behind MercyMe's beloved, chart topping song that brings ultimate hope to so many is a gripping reminder of the power of true forgiveness.</p>
</div>
<div class="pb-1 pl-3 pr-5">
<a href="../single-movies/single-movies-v3.html" class="btn btn-outline-white py-3 w-100 btn-sm" tabindex="0">WATCH NOW</a>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 px-2">
 <div class="position-relative mb-4 mb-lg-0 dark">
<div class="movie_poster">
<div>
<img class="img-fluid" src="assets/img/600x900/img7.jpg" alt="Image-Description">
</div>
</div>
<div class="position-absolute bottom-0">
<div class="pl-3 pr-5 pb-3">
<h6 class="text-center product-title font-size-3 mb-2 text-center">
<a href="../single-movies/single-movies-v3.html">Freak Show</a>
</h6>
<p class="text-white text-center font-size-1 max-h-42rem overflow-hidden">Follows the story of teenager Billy Bloom who, despite attending an ultra conservative high school, makes the decision to run for homecoming queen</p>
<div class="pb-1">
<a href="../single-movies/single-movies-v3.html" class="btn btn-outline-white py-3 w-100 btn-sm" tabindex="0">WATCH NOW</a>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 px-2">
<div class="position-relative dark">
<div class="movie_poster">
<div>
<img class="img-fluid" src="assets/img/600x900/img8.jpg" alt="Image-Description">
</div>
</div>
<div class="position-absolute bottom-0">
<div class="pl-3 pr-5 pb-3">
<h6 class="text-center product-title font-size-3 mb-2 text-center">
<a href="../single-movies/single-movies-v3.html">First We Take Brooklyn</a>
</h6>
<p class="text-white text-center font-size-1 max-h-42rem overflow-hidden">A troubled woman living in an isolated community finds herself pulled between the control of her oppressive family and the allure of a secretive outsider suspected of a series of brutal murders. Guided by their families, they enter the perilous word of politics and, in the process, learn a thing or two about love.</p>
<div class="pb-1">
<a href="../single-movies/single-movies-v3.html" class="btn btn-outline-white py-3 w-100 btn-sm" tabindex="0">WATCH NOW</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>-->
</main>


<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontfooter.php'; ?>



<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<button type="button" class="close position-absolute top-0 right-0 z-index-2 mt-3 mr-3" data-dismiss="modal" aria-label="Close">
<svg aria-hidden="true" class="mb-0" width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</button>

<div class="modal-body">
<form class="js-validate">

<div id="login">

<div class="text-center mb-7">
<h3 class="mb-0">Sign In to Vodi</h3>
<p>Login to manage your account.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signinEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-3">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signinPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
</div>
</div>

<div class="d-flex justify-content-end mb-4">
<a class="js-animation-link small link-underline" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#forgotPassword",
                                        "groupName": "idForm"
                                    }'>Forgot Password?
</a>
</div>
<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign In</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
 Sign In with Google
</span>
</a>
<div class="text-center">
<span class="small text-muted">Do not have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#signup",
                                        "groupName": "idForm"
                                    }'>Sign Up
</a>
</div>
</div>

<div id="signup" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Create your account</h3>
<p>Fill out the form to get started.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signupEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signupPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Confirm Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="confirmPassword" id="signupConfirmPassword" placeholder="Confirm Password" aria-label="Confirm Password" required data-msg="Password does not match the confirm password.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign Up</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
Sign Up with Google
</span>
</a>
<div class="text-center">
<span class="small text-muted">Already have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Sign In
</a>
</div>
</div>


<div id="forgotPassword" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Recover password</h3>
<p>Instructions will be sent to you.</p>
</div>


<div class="js-form-message">
<label class="sr-only" for="recoverEmail">Your email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="recoverEmail" placeholder="Your email" aria-label="Your email" required data-msg="Please enter a valid email address.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Recover Password</button>
</div>
<div class="text-center mb-4">
<span class="small text-muted">Remember your password?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Login
</a>
</div>
</div>

</form>
</div>

</div>
</div>
</div>


<aside id="sidebarContent" class="hs-unfold-content sidebar sidebar-left off-canvas-menu">
<div class="sidebar-scroller">
<div class="sidebar-container">
<div class="sidebar-footer-offset" style="padding-bottom: 7rem;">

<div class="d-flex justify-content-end align-items-center py-2 px-4 border-bottom">

<a class="navbar-brand mr-auto" href="../home/index.html" aria-label="Vodi">
<svg version="1.1" width="103" height="40px">
<linearGradient id="vodi-gr1" x1="0%" y1="0%" x2="100%" y2="0%">
<stop offset="0" style="stop-color:#2A4999"></stop>
<stop offset="1" style="stop-color:#2C9CD4"></stop>
</linearGradient>
<g class="vodi-gr">
<path class="vodi-svg0" d="M72.8,12.7c0-2.7,0-1.8,0-4.4c0-0.9,0-1.8,0-2.8C73,3,74.7,1.4,77,1.4c2.3,0,4.1,1.8,4.2,4.2c0,1,0,2.1,0,3.1
                                    c0,6.5,0,9.4,0,15.9c0,4.7-1.7,8.8-5.6,11.5c-4.5,3.1-9.3,3.5-14.1,0.9c-4.7-2.5-7.1-6.7-7-12.1c0.1-7.8,6.3-13.6,14.1-13.2
                                    c0.7,0,1.4,0.2,2.1,0.3C71.3,12.2,72,12.4,72.8,12.7z M67.8,19.8c-2.9,0-5.2,2.2-5.2,5c0,2.9,2.3,5.3,5.2,5.3
                                    c2.8,0,5.2-2.4,5.2-5.2C73,22.2,70.6,19.8,67.8,19.8z
                                    M39.9,38.6c-7.3,0-13.3-6.1-13.3-13.5c0-7.5,5.9-13.4,13.4-13.4c7.5,0,13.4,6,13.4,13.5
                                    C53.4,32.6,47.4,38.6,39.9,38.6z M39.9,30.6c3.2,0,5.6-2.3,5.6-5.6c0-3.2-2.3-5.5-5.5-5.5c-3.2,0-5.6,2.2-5.6,5.4
                                    C34.4,28.2,36.7,30.6,39.9,30.6z
                                    M14.6,27c0.6-1.4,1.1-2.6,1.6-3.8c1.2-2.9,2.5-5.8,3.7-8.8c0.7-1.7,2-2.8,4-2.7c3,0,4.9,2.6,3.8,5.4
                                    c-0.5,1.3-1.2,2.6-1.8,3.9c-2.4,5-4.9,10-7.3,15c-0.8,1.6-2,2.6-3.9,2.6c-2,0-3.3-0.8-4.2-2.6c-2.7-5.6-5.3-11.1-8-16.7
                                    c-0.3-0.7-0.6-1.3-0.9-2c-0.8-1.8-0.3-3.7,1.1-4.8c1.5-1.2,4-1.3,5.3,0c0.7,0.6,1.2,1.5,1.6,2.3C11.3,18.8,12.9,22.7,14.6,27z
                                    M90.9,25.1c0,3.1,0,6.2,0,9.4c0,1.9-1.2,3.4-2.9,4c-1.7,0.5-3.5,0-4.5-1.6c-0.5-0.8-0.8-1.8-0.8-2.6
                                    c-0.1-6.1-0.1-11.3,0-17.5c0-2.2,1.5-3.9,3.5-4.2c2.1-0.3,4.1,0.9,4.7,2.9c0.1,0.5,0.2,1.1,0.2,1.6C90.9,20,90.9,22.1,90.9,25.1
                                    C90.9,25.1,90.9,25.1,90.9,25.1z
                                    M90.2,4.7L86,2.3c-1.3-0.8-3,0.2-3,1.7v4.8c0,1.5,1.7,2.5,3,1.7l4.2-2.4C91.5,7.4,91.5,5.5,90.2,4.7z"></path>
</g>
</svg>
</a>

<div class="hs-unfold">
<a class="js-hs-unfold-invoker btn btn-icon btn-xs btn-soft-secondary" href="javascript:;" data-hs-unfold-options='{
                                    "target": "#sidebarContent",
                                    "type": "css-animation",
                                    "animationIn": "fadeInLeft",
                                    "animationOut": "fadeOutLeft",
                                    "hasOverlay": "rgba(55, 125, 255, 0.1)",
                                    "smartPositionOff": true
                                }'>
<svg width="10" height="10" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</a>
</div>
</div>


<div class="scrollbar sidebar-body">
<div class="sidebar-content py-4">

<div class="">
<div id="sidebarNavExample3" class="collapse show navbar-collapse">

<div class="sidebar-body_inner">
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav1Collapse" data-target="#sidebarNav1Collapse">
Home
</a>
<div id="sidebarNav1Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav1" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../home/index.html">Home v1</a>
 <a class="dropdown-item" href="../home/home-v2.html">Home v2</a>
<a class="dropdown-item" href="../home/home-v3.html">Home v3</a>
<a class="dropdown-item" href="../home/home-v4.html">Home v4</a>
<a class="dropdown-item" href="../home/home-v5.html">Home v5</a>
<a class="dropdown-item" href="../home/home-v6.html">Home v6 - Vodi Prime (Light)</a>
<a class="dropdown-item" href="../home/home-v7.html">Home v7 - Vodi Prime (Dark)</a>
<a class="dropdown-item" href="../home/home-v8.html">Home v8 - Vodi Stream</a>
<a class="dropdown-item" href="../home/home-v9.html">Home v9 - Vodi Tube (Light)</a>
<a class="dropdown-item" href="../home/home-v10.html">Home v10 - Vodi Tube (Dark)</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Collapse" data-target="#sidebarNav2Collapse">
Archive Pages
</a>
<div id="sidebarNav2Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav2" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../archive/movies.html">Movies</a>
<a class="dropdown-item" href="../archive/tv-shows.html">TV Shows</a>
<a class="dropdown-item" href="../archive/videos.html">Videos</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2One">
Single Movies
</a>
<div id="sidebarNav2One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-movies/single-movies-v1.html">Movie v1</a>
<a class="dropdown-item" href="../single-movies/single-movies-v2.html">Movie v2</a>
<a class="dropdown-item" href="../single-movies/single-movies-v3.html">Movie v3</a>
<a class="dropdown-item" href="../single-movies/single-movies-v4.html">Movie v4</a>
<a class="dropdown-item" href="../single-movies/single-movies-v5.html">Movie v5</a>
<a class="dropdown-item" href="../single-movies/single-movies-v6.html">Movie v6</a>
<a class="dropdown-item" href="../single-movies/single-movies-v7.html">Movie v7</a>
</div>
</div>
 <div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Two">
Single Videos
</a>
<div id="sidebarNav2Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-video/single-video-v1.html">Video v1</a>
<a class="dropdown-item" href="../single-video/single-video-v2.html">Video v2</a>
<a class="dropdown-item" href="../single-video/single-video-v3.html">Video v3</a>
<a class="dropdown-item" href="../single-video/single-video-v4.html">Video v4</a>
<a class="dropdown-item" href="../single-video/single-video-v5.html">Video v5</a>
<a class="dropdown-item" href="../single-video/single-video-v6.html">Video v6</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Collapse" data-target="#sidebarNav3Collapse">
Single Episodes
</a>
<div id="sidebarNav3Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav3" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../single-episodes/single-episodes-v1.html">Episode v1</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v2.html">Episode v2</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v3.html">Episode v3</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v4.html">Episode v4</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3One">
Other Pages
</a>
<div id="sidebarNav3One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../other/landing-v1.html">Landing v1</a>
<a class="dropdown-item" href="../other/landing-v2.html">Landing v2</a>
<a class="dropdown-item" href="../other/coming-soon.html">Coming Soon</a>
<a class="dropdown-item" href="../single-tv-show/single-tv-show.html">Single TV Show</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Two">
Blog Pages
</a>
<div id="sidebarNav3Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../blog/blog.html">Blog</a>
<a class="dropdown-item" href="../blog/blog-single.html">Single Blog</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4Collapse" data-target="#sidebarNav4Collapse">
Static Pages
</a>
<div id="sidebarNav4Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav4" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../static/contact.html">Contact Us</a>
<a class="dropdown-item" href="../static/404.html">404</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav4One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4One">
Docs
</a>
<div id="sidebarNav4One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav4">
<a class="dropdown-item" href="../../documentation/index.html">Documentation</a>
<a class="dropdown-item" href="../../snippets/index.html">Snippets</a>
</div>
</div>

</div>
</div>
</div>

</div>
</div>

</div>
</div>
</div>
</aside>



<a class="js-go-to go-to position-fixed" href="javascript:;" style="visibility: hidden;" data-hs-go-to-options='{
            "offsetTop": 700,
            "position": {
                "init": {
                    "right": 15
                },
                "show": {
                    "bottom": 15
                },
                "hide": {
                    "bottom": -15
                }
            }
        }'>
<i class="fas fa-angle-up"></i>
</a>

<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontscript.php'; ?>
</body>
</html>
